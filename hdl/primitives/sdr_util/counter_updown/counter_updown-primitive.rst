.. counter_updown documentation

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


.. _counter_updown-primitive:


Up down counter (``counter_updown``)
====================================
A counter that can load in a value and increment or decrement it by one.

Design
------
This is a counter that will increment or decrement its current value by one depending upon the direction that has been set.

Implementation
--------------
Inputs are read into the primitive on the rising edge of ``clk``. Outputs are set on rising edge of ``clk``. ``reset`` provides a synchronous, active high, reset signal to the module.

The counter value is set to zero on a reset.

The primitive is designed so that loading a value in takes priority. To load a desired value in put the value on the ``load_value`` port and set ``load_trig`` to ``1``. With ``load_trig`` set to ``1`` the counter will not increment or decrement.

The direction of the counter is determined by the input on the ``direction`` port. If set to ``1`` the counter will be set to increment, if set to ``0`` the counter will be set to decrement.

To enable counting the ``enable`` port must be set to ``1``. When ``enable`` is set to ``1`` the counter will increment or decrement on every rising clock edge.

Interface
---------

Generics
~~~~~~~~

 * ``counter_size_g`` (``integer``): Bit width of counter output.

Ports
~~~~~

 * ``clk`` (``std_logic``), in: Clock. Inputs and outputs registered on rising edge.

 * ``reset`` (``std_logic``), in: Reset. Active high, synchronous with rising edge of clock.

 * ``enable`` (``std_logic``), in: Primitive begins to count each rising clock edge when high.

 * ``direction`` (``std_logic``), in: Counter counts up when direction is 1, counts down when direction is 0.

 * ``load_trig`` (``std_logic``), in: When high the counter sets its counter value to the ``load_value`` input.

 * ``load_value`` (``unsigned``, ``counter_size_g`` bits), in: The value to be loaded into the counter value.

 * ``length`` (``unsigned``, ``counter_size_g`` bits), in: When counting up this is the number of times the counter will count before wrapping back to 0. When counting down, this is the value the counter is reset to after reaching 0.

 * ``count_out`` (``unsigned``, ``counter_size_g`` bits), out: Outputs the counter value.

Dependencies
------------
The dependencies to other elements in OpenCPI are:

 * None.

There is also a dependency on:

 * ``ieee.std_logic_1164``

 * ``ieee.numeric_std``

Limitations
-----------
Limitations of ``counter_updown`` are:

 * To use the maximum value of ``length`` it must be set to 0.
