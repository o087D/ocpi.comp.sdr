-- Communications Package
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

-- This package enables VHDL code to instantiate all entities and modules
-- in this library

library ieee;
use IEEE.std_logic_1164.all;
use ieee.numeric_std.all;

package sdr_communication is

  component crc_parallel
    generic (
      input_size_g     : integer          := 8;
      polynomial_g     : std_logic_vector := X"8005";
      input_reflect_g  : std_logic        := '0';
      output_reflect_g : std_logic        := '0'
      );
    port (
      clk       : in  std_logic;
      rst       : in  std_logic;
      clk_en    : in  std_logic;
      data_in   : in  std_logic_vector(input_size_g - 1 downto 0);
      seed      : in  std_logic_vector(polynomial_g'length - 1 downto 0);
      final_xor : in  std_logic_vector(polynomial_g'length - 1 downto 0);
      crc_out   : out std_logic_vector(polynomial_g'length - 1 downto 0)
      );
  end component;

  component crc_serial
    generic (
      polynomial_g     : std_logic_vector := X"8005";
      output_reflect_g : std_logic        := '0'
      );
    port (
      clk       : in  std_logic;
      rst       : in  std_logic;
      clk_en    : in  std_logic;
      data_in   : in  std_logic;
      seed      : in  std_logic_vector(polynomial_g'length - 1 downto 0);
      final_xor : in  std_logic_vector(polynomial_g'length - 1 downto 0);
      crc_out   : out std_logic_vector(polynomial_g'length - 1 downto 0)
      );
  end component;

  component prbs_generator
    generic (
      LFSR_WIDTH_G : positive := 31;
      LFSR_TAP_1_G : natural  := 28;
      LFSR_TAP_2_G : natural  := 0;
      LFSR_TAP_3_G : natural  := 0;
      LFSR_TAP_4_G : natural  := 0;
      LFSR_TAP_5_G : natural  := 0
      );
    port (
      clk        : in  std_logic;
      reset      : in  std_logic;
      clk_en     : in  std_logic;
      invert_i   : in  std_logic;
      initial_i  : in  std_logic_vector(LFSR_WIDTH_G - 1 downto 0);
      data_o     : out std_logic_vector(LFSR_WIDTH_G - 1 downto 0);
      data_msb_o : out std_logic
      );
  end component;

  component prbs_synchroniser is
    generic (
      LFSR_WIDTH_G : positive := 31;
      LFSR_TAP_1_G : natural  := 28;
      LFSR_TAP_2_G : natural  := 0;
      LFSR_TAP_3_G : natural  := 0;
      LFSR_TAP_4_G : natural  := 0;
      LFSR_TAP_5_G : natural  := 0
      );
    port (
      clk                     : in  std_logic;
      reset                   : in  std_logic;
      clk_en                  : in  std_logic;
      invert_i                : in  std_logic;
      data_bit_i              : in  std_logic;
      check_period_i          : in  unsigned(15 downto 0);
      error_threshold_i       : in  unsigned(15 downto 0);
      bits_checked_counter_o  : out unsigned(63 downto 0);
      bit_error_counter_o     : out unsigned(63 downto 0);
      bits_received_counter_o : out unsigned(63 downto 0);
      sync_loss_counter_o     : out unsigned(63 downto 0);
      data_o                  : out std_logic_vector(7 downto 0)
      );
  end component;

  component multiplicative_scrambler is
    generic (
      polynomial_g : std_logic_vector := "11101"
      );
    port (
      clk            : in  std_logic;
      rst            : in  std_logic;
      seed           : in  std_logic_vector(polynomial_g'length - 2 downto 0);
      data_valid_in  : in  std_logic;
      data_in        : in  std_logic;
      data_valid_out : out std_logic;
      data_out       : out std_logic
      );
  end component;

  component multiplicative_descrambler is
    generic (
      polynomial_g : std_logic_vector := "11101"
      );
    port (
      clk            : in  std_logic;
      rst            : in  std_logic;
      seed           : in  std_logic_vector(polynomial_g'length - 2 downto 0);
      data_valid_in  : in  std_logic;
      data_in        : in  std_logic;
      data_valid_out : out std_logic;
      data_out       : out std_logic
      );
  end component;

  component bit_stuffer is
    generic (
      consecutive_ones_g : integer := 5
      );
    port (
      clk            : in  std_logic;
      rst            : in  std_logic;
      enable         : in  std_logic;
      ready          : out std_logic;
      all_ones       : out std_logic;
      data_valid_in  : in  std_logic;
      data_in        : in  std_logic;
      data_valid_out : out std_logic;
      data_out       : out std_logic
      );
  end component;

end package sdr_communication;
