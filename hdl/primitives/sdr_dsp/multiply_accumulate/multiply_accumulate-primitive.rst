.. multiply_accumulate documentation

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


.. _multiply_accumulate-primitive:


Multiply accumulator (``multiply_accumulate``)
==============================================
Calculates the product of two numbers and adds the result to an accumulator.

Design
------
The mathematical representation of the implementation is given in :eq:`multiply_accumulate-equation`.

.. math::
   :label: multiply_accumulate-equation

   y[n] = y[n - 1] + a[n]*b[n]

In :eq:`multiply_accumulate-equation`:

 * :math:`y[n]` is the output values, when :math:`n < 0` then :math:`y[n]` is assumed to be zero.

 * :math:`a[n]` and :math:`b[n]` are the input values.

Implementation
--------------
The MAC primitive is designed to operating in a way that is compatible with FPGA DSP slices from most major manufacturers.

The Xilinx DSP48E1 slice allows for up to a 25 x 18 multiply with 48 bit accumulator while the Intel Cyclone / Arria V allows for up to 27 x 27 (or dual 18 x 19) multiply with a 64 bit accumulator.

The accumulator is initialised to zero on start-up and can perform a MAC operation on every rising edge of the input clock.

There is a latency of two clock cycles between the input of two values to multiply and the result of the MAC operation appearing at ``accum_out`` .

The ``load_accum`` signal allows the value stored in the accumulator to be replaced with a new value (``accum_in``) or to be reset to zero.

Interface
---------

Generics
~~~~~~~~

 * ``a_in_size_g`` (``integer``): Bit width of the ``a_in`` signal.

 * ``b_in_size_g`` (``integer``): Bit width of the ``b_in`` signal.

 * ``accumulator_size_g`` (``integer``): Bit width of the accumulator signal.

Ports
~~~~~

 * ``clk`` (``std_logic``), in: Clock. Inputs and outputs registered on rising edge.

 * ``rst`` (``std_logic``), in: Reset. Active high, synchronous with rising edge of clock.

 * ``en_in`` (``std_logic``), in: Enable. Primitive enabled when high.

 * ``a_in`` (``signed``, ``a_in_size_g`` bits), in: Input 1 to the multiplier of the MAC unit.

 * ``b_in`` (``signed``, ``b_in_size_g`` bits), in: Input 2 to the multiplier of the MAC unit.

 * ``load_accum`` (``std_logic``), in: When high the accumulator is set to the value of ``accum_in``.

 * ``accum_in`` (``signed``, ``accumulator_size_g`` bits), in: When ``load_accum`` is high the accumulator is set to the value of ``accum_in``.

 * ``accum_out`` (``signed``, ``accumulator_size_g`` bits), out: The current value of the accumulator in the MAC unit.

Dependencies
------------
The dependencies to other elements in OpenCPI are:

 * None.

There is also a dependency on:

 * ``ieee.std_logic_1164``

 * ``ieee.numeric_std``

Limitations
-----------
Limitations of ``multiply_accumulate`` are:

 * None.
