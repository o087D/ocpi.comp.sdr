.. cordic_rec_to_pol documentation

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


.. meta::
   :keywords: sinusoidal wave trigonometry


.. _cordic_rec_to_pol-primitive:


CORDIC rectangular to polar conversion (``cordic_rec_to_pol``)
==============================================================
Converts values from a rectangular to polar coordinate system, using the CORDIC algorithm.

Design
------
Calculates the mathematical function :math:`A * \text{atan2}\left( y, x \right)` using the Coordinate Rotation Digital Computer (CORDIC) method. Additionally the function :math:`A*\sqrt{x^2 + y^2}` is also calculated.

The primitive can be used to convert from rectangular to polar representation of a complex number where the complex number is defined as :math:`x + jy`.

The definition of the :math:`\text{atan2}\left( y, x \right)` function used here is given in :eq:`atan2-equation`.

.. math::
   :label: atan2-equation

   atan2(Y,X) = \begin{cases}
             \text{arctan}\left (\frac {y}{x}\right ) &  \text {if } x > 0 \\
             \text{arctan}\left (\frac {y}{x}\right ) + \pi &  \text {if } x < 0 \text{ and } y \geq 0 \\
             \text{arctan}\left (\frac {y}{x}\right ) - \pi &  \text {if } x < 0 \text{ and } y < 0 \\
             +\frac {\pi}{2} &  \text {if } x = 0 \text{ and } y > 0 \\
             -\frac {\pi}{2} &  \text {if } x = 0 \text{ and } y < 0 \\
             \text{undefined} & \text {if } x = 0 \text{ and } y = 0
          \end{cases}
   :label: atan2-equ

The output of the :math:`\text{atan2}\left(y, x \right)` function is in the range :math:`\left[ -\pi, +\pi \right]`.

In this primitive, calculation of the :math:`\text{atan2}` is scaled by a factor of :math:`\frac {(2^{\texttt{output_size_g} - 1} - 1)}{\pi}`. Meaning ``angle_out``, which is an output of this primitive, is given by :eq:`cordic_rec_to_pol-equation`.

.. math::
   :label: cordic_rec_to_pol-equation

   \texttt{angle_out} = \frac {(2^{\texttt{output_size_g} - 1} - 1)}{\pi} \text{atan2}\left(y, x \right)

The output of the square root calculation is returned on the ``magnitude_out`` port. This is given by :eq:`cordic_magnitude-equation`.

.. math::
   :label: cordic_magnitude-equation

   \text{magnitude_out} = G_{cordic} \sqrt{x^2 + y^2}

In :eq:`cordic_magnitude-equation`, :math:`G_{cordic}` is the gain introduced by the CORDIC algorithm. The gain caused by the CORDIC algorithm is given by :eq:`cordic_gain-equation`.

.. math::
   :label: cordic_gain-equation

   G_{cordic} = \prod_{i=0}^{i=\texttt{output_size_g} - 1} \frac{1}{\sqrt{1+2^{-2i}}}

In :eq:`cordic_gain-equation` when ``output_size_g`` is greater than or equal to 16, :math:`G_{cordic}` can be assumed to be a constant of 1.646760258.

Implementation
--------------
The primitive is pipelined so that a new sample can be inserted into the primitive on every rising edge of ``clk``. The ``valid_in`` signal allows each input sample to be marked as valid or not. The pipeline advances every rising edge of ``clk`` regardless of if valid data is passed to the primitive or not.

The ``valid_out`` signal is a delayed version of ``valid_in`` that indicates which outputs were calculated based on valid inputs. There is a latency of ``output_size_g`` clock cycles between the input of ``x_in`` and ``y_in`` the calculation of ``angle_out`` and ``magnitude_out``.

The ``enable`` signal provides a clock enable for the primitive. When ``enable`` is set low no data will enter or leave the primitive and all calculations are stopped. When ``enable`` is high the module will operate normally.

The primitive includes a look up table that works for ``output_size_g`` up to 32 bits wide. Larger output are supported but require a new look up table to be calculated.

``output_size_g`` refers only to the width of the ``angle_out`` output. The width of ``magnitude_out`` is fixed to be 2 bits larger than ``input_size_g``, as this ensures that no overflows occur due to the CORDIC gain.

Interface
---------

Generics
~~~~~~~~

 * ``input_size_g`` (``integer``): Input bit width of :math:`x` and :math:`y` values.

 * ``output_size_g`` (``integer``): Output bit width of :math:`\text{atan2}` function. This has a maximum limit of 32 bits, unless the CORDIC lookup table is recalculated.

Ports
~~~~~

 * ``clk`` (``std_logic``), in: Clock. Inputs and outputs registered on rising edge.

 * ``rst`` (``std_logic``), in: Reset. Active high, synchronous with rising edge of clock.

 * ``enable`` (``std_logic``), in: Enable. Primitive enabled when high.

 * ``valid_in`` (``std_logic``), in: Valid in. ``x_in`` and ``y_in`` are valid when ``valid_in`` is high.

 * ``x_in`` (``signed``, ``input_size_g`` bits), in: Input :math:`x` value.

 * ``y_in`` (``signed``, ``input_size_g`` bits), in: Input :math:`y` value.

 * ``valid_out`` (``std_logic``), out: Valid Out. ``angle_out`` and ``magnitude_out`` are valid when ``valid_out`` is high.

 * ``angle_out`` (``signed``, ``output_size_g`` bits), out: Angle output. Output of :math:`\text{\atan2}` calculation.

 * ``magnitude_out`` (``signed``, ``input_size_g + 1`` bits), out: Magnitude output. Output square root calculation.

Dependencies
------------
The dependencies to other elements in OpenCPI are:

 * None.

There is also a dependency on:

 * ``ieee.std_logic_1164``

 * ``ieee.numeric_std``

Limitations
-----------
Limitations of ``cordic_rec_to_pol`` are:

 * Due to accumulating rounding errors when using fixed point maths, for very small values for ``x_in`` and  ``y_in`` this primitive will give a low accuracy output. Figure :numref:`cordic_rec_to_pol_deviation-diagram` plots the maximum absolute deviation of the implementation compared to the expected value for combinations of small input values.

.. _cordic_rec_to_pol_deviation-diagram:

.. figure:: cordic_rec_to_pol_deviation.svg
   :alt: Maximum absolute deviation of CORDIC rectangular to polar primitive
   :align: center

   Maximum absolute deviation of CORDIC rectangular to polar primitive.

In :numref:`cordic_rec_to_pol_deviation-diagram`, the deviation is found for each of :math:`a + jn`, :math:`n + ja`, :math:`(n + a) + jn` and :math:`n + j(n + a)`, and for all integer values of :math:`a` in the range :math:`-3 \leq a \leq 3`. The maximum deviation found for :math:`n` for each of seen for all of these conditions is then plotted.
