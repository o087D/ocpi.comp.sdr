-- square_scaled_s HDL implementation.
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

library IEEE;
use IEEE.std_logic_1164.all;
use ieee.numeric_std.all;
library ocpi;
use ocpi.types.all;

architecture rtl of square_scaled_s_worker is

  -- Scale output as a power of two
  constant scale_output_c : integer := to_integer(scale_output);

  -- Constants
  constant data_in_width_c      : integer := 16;
  constant square_out_width_c   : integer := data_in_width_c * 2;
  constant delay_c              : integer := 1;
  -- Input interface signals
  signal give                   : std_logic;
  signal enable                 : std_logic;
  signal take                   : std_logic;
  -- Square result
  signal square_data_out        : signed(square_out_width_c - 1 downto 0);
  signal square_data_out_scaled : signed(output_out.data'length - 1 downto 0);
  signal data_out_slv           : std_logic_vector(output_out.data'length - 1 downto 0);

begin

  -- Take input data whenever both the input and the output are ready
  take           <= input_in.ready and output_in.ready and ctl_in.is_operating;
  input_out.take <= take;
  enable         <= ctl_in.is_operating and output_in.ready;

  square_scaled_p : process(ctl_in.clk)
  begin
    if rising_edge(ctl_in.clk) then
      if (enable = '1') then
        square_data_out <= signed(signed(input_in.data) * signed(input_in.data));
      end if;
    end if;
  end process;

  -- Interface delay

  interface_delay_i : entity work.short_protocol_delay
    generic map (
      DELAY_G              => delay_c,
      DATA_IN_WIDTH_G      => input_in.data'length,
      PROCESSED_DATA_MUX_G => '1'
      )
    port map (
      clk                 => ctl_in.clk,
      reset               => ctl_in.reset,
      enable              => enable,
      take_in             => take,
      input_in            => input_in,
      processed_stream_in => data_out_slv,
      output_out          => output_out
      );

  square_data_out_scaled <= square_data_out(scale_output_c + 15 downto scale_output_c);
  data_out_slv           <= std_logic_vector(square_data_out_scaled);

end rtl;
