-- abs_s HDL implementation.
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

library IEEE;
use IEEE.std_logic_1164.all;
use ieee.numeric_std.all;
library ocpi;
use ocpi.types.all;

architecture rtl of abs_s_worker is

  constant delay_c : integer := 1;

  signal multi_out : std_logic_vector(output_out.data'length - 1 downto 0);
  signal give      : std_logic;
  signal take      : std_logic;
  signal enable    : std_logic;

begin

  -- Take input data whenever both the input and the output are ready
  take           <= input_in.ready and output_in.ready and ctl_in.is_operating;
  input_out.take <= take;
  enable         <= ctl_in.is_operating and output_in.ready;

  -- Interface delay module
  -- Delays streaming interface signals to align with the delay introduced by
  -- the processing module.
  interface_delay_i : entity work.short_protocol_delay
    generic map (
      DELAY_G              => delay_c,
      DATA_IN_WIDTH_G      => input_in.data'length,
      PROCESSED_DATA_MUX_G => '1'
      )
    port map (
      clk                 => ctl_in.clk,
      reset               => ctl_in.reset,
      enable              => enable,
      take_in             => take,
      input_in            => input_in,
      processed_stream_in => multi_out,
      output_out          => output_out
      );

  -- Take absolute value
  abs_s_p : process(ctl_in.clk)
  begin
    if rising_edge(ctl_in.clk) then
      if enable = '1' then
        if signed(input_in.data) = -32768 then
          multi_out <= (others => '0');
        else
          multi_out <= std_logic_vector(abs(signed(input_in.data)));
        end if;
      end if;
    end if;
  end process;

end rtl;
