-- multiplier_s_l HDL implementation.
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

library IEEE;
use IEEE.std_logic_1164.all;
use ieee.numeric_std.all;
library ocpi;
use ocpi.types.all;

architecture rtl of multiplier_s_l_worker is

  constant delay_c : integer := 1;

  signal enable         : std_logic;
  signal input_1_take   : std_logic;
  signal input_2_take   : std_logic;
  signal multiplier_out : std_logic_vector(output_out.data'length - 1 downto 0);
  signal output         : worker_output_out_t;

begin

  enable <= output_in.ready and ctl_in.is_operating;

  input_1_out.take <= input_1_take;
  input_2_out.take <= input_2_take;

  -- Always take non stream opcodes from both inputs.
  -- Only take stream opcodes if both ports have a stream opcode.
  input_take_logic_p : process (enable, input_1_in, input_2_in)
  begin
    -- INPUT 1
    -- If input and output are ready
    if (enable = '1' and input_1_in.ready = '1') then
      -- Always take when a non-stream opcode or not valid
      if input_1_in.opcode /= short_timed_sample_sample_op_e or input_1_in.valid = '0' then
        input_1_take <= '1';
      -- Only take a stream opcode if input 2 is ready, valid,
      -- and also a stream opcode
      elsif input_2_in.ready = '1' and input_2_in.valid = '1' and
        input_2_in.opcode = short_timed_sample_sample_op_e then
        input_1_take <= '1';
      else
        input_1_take <= '0';
      end if;
    else
      input_1_take <= '0';
    end if;
    -- INPUT 2
    -- If input and output are ready
    if (enable = '1' and input_2_in.ready = '1') then
      -- Always take when a non-stream opcode or not valid
      if input_2_in.opcode /= short_timed_sample_sample_op_e or input_2_in.valid = '0' then
        input_2_take <= '1';
      -- Only take a stream opcode if input 1 is ready, valid, and also a
      -- stream opcode.
      elsif input_1_in.ready = '1' and input_1_in.valid = '1' and
        input_1_in.opcode = short_timed_sample_sample_op_e then
        input_2_take <= '1';
      else
        input_2_take <= '0';
      end if;
    else
      input_2_take <= '0';
    end if;
  end process;

  -- Interface delay module
  -- Delays streaming interface signals to align with the delay introduced by
  -- the processing module.
  interface_delay_i : entity work.short_to_long_protocol_delay
    generic map (
      DELAY_G          => delay_c,
      DATA_IN_WIDTH_G  => input_1_in.data'length,
      DATA_OUT_WIDTH_G => output_out.data'length
      )
    port map (
      clk                 => ctl_in.clk,
      reset               => ctl_in.reset,
      enable              => enable,
      take_in             => input_1_take,
      input_in            => input_1_in,
      processed_stream_in => multiplier_out,
      output_out          => output
      );

  -- Performs multiplication operation
  multiplier_s_l_p : process(ctl_in.clk)
  begin
    if rising_edge(ctl_in.clk) then
      if enable = '1' then

        multiplier_out <= std_logic_vector(signed(input_1_in.data) * signed(input_2_in.data));

      end if;
    end if;
  end process;

  -- Split stream messages over OCPI_MAX_BYTES_OUTPUT / 4 words, as this is the
  -- maximum size specified by the protocol.
  limit_message_size_i : entity work.long_protocol_message_limiter
    generic map(
      DATA_OUT_WIDTH_G     => output_out.data'length,
      MAX_MESSAGE_LENGTH_G => to_integer(OCPI_MAX_BYTES_OUTPUT) / 4
      )
    port map(
      clk                => ctl_in.clk,
      reset              => ctl_in.reset,
      enable             => enable,
      output_out         => output,
      output_out_limited => output_out
      );

end rtl;
