.. complex_multiplier_xf documentation

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


.. _complex_multiplier_xf:


Complex multiplier (``complex_multiplier_xf``)
==============================================
Multiplies two complex values together.

Design
------
The mathematical representation of the implementation is given in :eq:`complex_multiplier_xf-equation`.

.. math::
   :label: complex_multiplier_xf-equation

   z[n] = x[n] * y[n]

In :eq:`complex_multiplier_xf-equation`:

 * :math:`x[n]` and math:`y[n]` are the input values.

 * :math:`y[n]` is the output values.

A block diagram representation of the implementation is given in :numref:`complex_multiplier_xf-diagram`.

.. _complex_multiplier_xf-diagram:

.. figure:: complex_multiplier_xf.svg
   :alt: Block diagram outlining complex multiplier implementation.
   :align: center

Interface
---------
.. literalinclude:: ../specs/complex_multiplier_xf-spec.xml
   :language: xml
   :lines: 1,19-

Ports
~~~~~
.. ocpi_documentation_ports::

   input_1: Input samples port.
   input_2: Input samples port.
   output: Primary output samples port.

Opcode handling
~~~~~~~~~~~~~~~
Multiplication is only applied to data in sample opcode messages. All other opcode messages received on ``input_1`` are passed through this component without any effect. All messages with a non-sample opcode received on ``input_2`` are discarded.

Sample message lengths received on ``input_1`` are persevered on the output. Sample message lengths received on ``input_2`` are ignored.

Properties
~~~~~~~~~~
.. ocpi_documentation_properties::

Parameters
~~~~~~~~~~
.. ocpi_documentation_properties::
   :parameters:

Implementations
---------------
.. ocpi_documentation_implementations:: ../complex_multiplier_xf.rcc

Example application
-------------------
.. literalinclude:: example_app.xml
   :language: xml
   :lines: 1,19-

Dependencies
------------
The dependencies to other elements in OpenCPI are:

 * None.

There is also a dependency on:

 * None.

Limitations
-----------
Limitations of ``complex_multiplier_xf`` are:

 * If any intermediate value of the calculation of the output value exceeds the supported range of a single precision floating point number, the output will be a floating point special value (``+inf``, ``-inf`` or ``NaN``), even if the 'correct' value would be within the supported single precision floating point range. For example, if :math:`x = a + jb` is to be multiplied by :math:`y = c +jd`, then :math:`xy = (ac - bd) + j(ad + bc)` will contain a floating point special value, if any of :math:`ac`, :math:`bd`, :math:`ad` or :math:`bc` are outside of the supported range of a single prevision floating point number. Even if :math:`ac - bd` or :math:`ad + bc` calculated directly are within the supported range of a single precision floating point number.

Testing
-------
.. ocpi_documentation_test_result_summary::
