.. log_us HDL worker

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


:orphan:

.. _log_us-HDL-worker:


``log_us`` HDL worker
=====================
Pipelined HDL implementation of logarithm, allowing one input and one output every FPGA clock cycle, with a delay of 24 clock cycles through the pipeline.

Detail
------
The logarithm primitive produces the logarithm result. The result of the logarithm primitive is scaled by a power of two. The output of the scaling is rounded to the nearest integer by the rounding half-up primitive. As the logarithm result is a 16-bit fixed point value, setting the output scaling factor greater than 16 adds no further precision to the output.

The implementation interprets input values of 0 as a 1. Output values are limited to 65535 if exceeded.

.. ocpi_documentation_worker::

Utilisation
-----------
.. ocpi_documentation_utilisation::
