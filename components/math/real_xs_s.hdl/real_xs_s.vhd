-- Real value from a complex input data stream implementation.
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

library IEEE;
use IEEE.std_logic_1164.all;
use ieee.numeric_std;
library ocpi;
use ocpi.types.all;

architecture rtl of real_xs_s_worker is

  constant delay_c      : integer := 1;
  constant input_size_c : integer := 16;

  signal real_out     : std_logic_vector(input_size_c - 1 downto 0) := (others => '0');
  signal give         : std_logic;
  signal take         : std_logic;
  signal enable       : std_logic;
  signal output_ready : std_logic;
  signal input_hold   : std_logic;
  signal data_in_i    : std_logic_vector(input_size_c - 1 downto 0) := (others => '0');

begin

  output_ready <= ctl_in.is_operating and output_in.ready;

  -- Take input data whenever both the input and the output are ready
  take           <= input_in.ready and output_ready and not input_hold;
  input_out.take <= take;

  -- Only enable processing when the output is ready and input_hold is low.
  -- Input hold is high when non-stream opcodes with valid data are being
  -- output from the component, as this takes two clock cycles per input due to
  -- the difference in input and output width.
  enable <= output_in.ready and not input_hold;

  -- Split I and Q signals to allow return of real component of stream
  data_in_i <= input_in.data(input_size_c - 1 downto 0);

  -- Interface delay module
  -- Delays streaming interface signals to align with the delay introduced by
  -- the processing module.
  interface_delay_i : entity work.complex_short_to_short_protocol_delay
    generic map (
      DELAY_G          => delay_c,
      DATA_IN_WIDTH_G  => input_in.data'length,
      DATA_OUT_WIDTH_G => output_out.data'length
      )
    port map (
      clk                 => ctl_in.clk,
      reset               => ctl_in.reset,
      enable              => output_ready,
      take_in             => take,
      input_in            => input_in,
      processed_stream_in => real_out,
      output_out          => output_out,
      input_hold_out      => input_hold
      );

  -- Perform real operation
  real_xs_s_p : process(ctl_in.clk)
  begin
    if rising_edge(ctl_in.clk) then
      if enable = '1' then
        real_out <= data_in_i;
      end if;
    end if;
  end process;

end rtl;
