-- adder_l HDL implementation.
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

library IEEE;
use IEEE.std_logic_1164.all;
use ieee.numeric_std.all;
library ocpi;
use ocpi.types.all;

architecture rtl of adder_l_worker is

  constant delay_c      : integer := 1;
  constant input_size_c : integer := 32;

  signal enable            : std_logic;
  signal input_1_take      : std_logic;
  signal input_2_take      : std_logic;
  signal input_1_processed : signed(input_size_c - 1 downto 0);
  signal input_2_processed : signed(input_size_c - 1 downto 0);
  signal adder_out         : signed(31 downto 0);
  signal adder_out_slv     : std_logic_vector(31 downto 0);

begin

  enable <= output_in.ready and ctl_in.is_operating;

  input_1_out.take <= input_1_take;
  input_2_out.take <= input_2_take;

  -- Always take non stream opcodes from both inputs.
  -- Only take stream opcodes if both ports have a stream opcode.
  input_take_logic_p : process (enable, input_1_in, input_2_in)
  begin
    -- INPUT 1
    -- If input and output are ready
    if (enable = '1' and input_1_in.ready = '1') then
      -- Always take when a non-stream opcode or not valid
      if input_1_in.opcode /= long_timed_sample_sample_op_e or input_1_in.valid = '0' then
        input_1_take <= '1';
      -- Only take a stream opcode if input 2 is ready, valid,
      -- and also a stream opcode
      elsif input_2_in.ready = '1' and input_2_in.valid = '1' and
        input_2_in.opcode = long_timed_sample_sample_op_e then
        input_1_take <= '1';
      else
        input_1_take <= '0';
      end if;
    else
      input_1_take <= '0';
    end if;
    -- INPUT 2
    -- If input and output are ready
    if (enable = '1' and input_2_in.ready = '1') then
      -- Always take when a non-stream opcode or not valid
      if input_2_in.opcode /= long_timed_sample_sample_op_e or input_2_in.valid = '0' then
        input_2_take <= '1';
      -- Only take a stream opcode if input 1 is ready, valid, and also a
      -- stream opcode.
      elsif input_1_in.ready = '1' and input_1_in.valid = '1' and
        input_1_in.opcode = long_timed_sample_sample_op_e then
        input_2_take <= '1';
      else
        input_2_take <= '0';
      end if;
    else
      input_2_take <= '0';
    end if;
  end process;

  -- Interface delay module
  -- Delays streaming interface signals to align with the delay introduced by
  -- the processing module.
  interface_delay_i : entity work.long_protocol_delay
    generic map (
      DELAY_G              => delay_c,
      DATA_IN_WIDTH_G      => input_1_in.data'length,
      PROCESSED_DATA_MUX_G => '1'
      )
    port map (
      clk                 => ctl_in.clk,
      reset               => ctl_in.reset,
      enable              => enable,
      take_in             => input_1_take,
      input_in            => input_1_in,
      processed_stream_in => adder_out_slv,
      output_out          => output_out
      );

  -- Perform adder operation (with optional negate on each input)
  adder_l_p : process(ctl_in.clk)
    variable negate : std_logic_vector(1 downto 0);
  begin
    negate := props_in.negate_input_2 & props_in.negate_input_1;
    if rising_edge(ctl_in.clk) then
      if enable = '1' then
        case negate is
          when "00"   => adder_out <= signed(input_1_in.data)+signed(input_2_in.data);
          when "01"   => adder_out <= signed(input_2_in.data)-signed(input_1_in.data);
          when "10"   => adder_out <= signed(input_1_in.data)-signed(input_2_in.data);
          when others => adder_out <= -signed(input_1_in.data)-signed(input_2_in.data);
        end case;
      end if;
    end if;
  end process;
  adder_out_slv <= std_logic_vector(adder_out);
end rtl;
