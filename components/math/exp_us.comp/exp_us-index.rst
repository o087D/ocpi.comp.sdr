.. exp_us documentation

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


.. meta::
   :keywords: exp


.. _exp_us:


Exponentiation (``exp_us``)
===========================
Calculates a base value raised to the power of the input sample opcode message values.

Design
------
Input values are first scaled by an factor of :math:`2^N`, where :math:`N` is the ``scale_input`` property. A ``base`` value is then raised to the power of this scaled input value.

The mathematical representation of the implementation is given in :eq:`exp_us-equation`.

.. math::
   :label: exp_us-equation

   y[n] = b^{frac{x[n]}{2^N}}

In :eq:`exp_us-equation`:

 * :math:`x[n]` is the input values.

 * :math:`b` is the base value, this is set by the ``base`` property.

 * :math:`N` is the input scale power, this is set by the ``scale_input`` property.

 * :math:`y[n]` is the output values, before rounding. Values that are returned by the component are first rounded using a :ref:`half up rounder <rounding_halfup-primitive>`.

A block diagram representation of the implementation is given in :numref:`exp_us-diagram`.

.. _exp_us-diagram:

.. figure:: exp_us.svg
   :alt: Block diagram outlining exponentiation implementation.
   :align: center

   Exponentiation implementation.

To avoid undefined implementation behaviour the input scaling factor must be set high enough to ensure the output never exceeds 65535.

Interface
---------
.. literalinclude:: ../specs/exp_us-spec.xml
   :language: xml
   :lines: 1,19-

Ports
~~~~~
.. ocpi_documentation_ports::

   input: Primary input samples port.
   output: Primary output samples port.

Opcode handling
~~~~~~~~~~~~~~~
The exponentiation operation is only applied to values in a sample opcode message. All other opcodes messages received are passed through this component without any effect.

Properties
~~~~~~~~~~
.. ocpi_documentation_properties::

Parameters
~~~~~~~~~~
.. ocpi_documentation_properties::
   :parameters:

Implementations
---------------
.. ocpi_documentation_implementations:: ../exp_us.hdl ../exp_us.rcc

Example application
-------------------
.. literalinclude:: example_app.xml
   :language: xml
   :lines: 1,19-

Dependencies
------------
The dependencies to other elements in OpenCPI are:

 * :ref:`Exponentiation primitive <exponentiation-primitive>`

 * :ref:`Rounding halfup primitive <rounding_halfup-primitive>`

 * :ref:`Protocol interface delay primitive <protocol_interface_delay-primitive>`

There is also a dependency on:

 * ``ieee.std_logic_1164``

 * ``ieee.numeric_std``

 * ``ieee.math_real``

Limitations
-----------
Limitations of ``exp_us`` are:

 * Base values less than or equal to one are not supported.

 * Input values that result in output values greater than 65535 give undefined behaviour.

Testing
-------
.. ocpi_documentation_test_result_summary::
