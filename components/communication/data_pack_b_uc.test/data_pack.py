#!/usr/bin/env python3

# Python implementation of data_pack
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.


import decimal

import numpy as np

import ocpi_testing


class DataPack(ocpi_testing.Implementation):
    def __init__(self, msb_first, flush_enable):
        super().__init__(
            msb_first=msb_first, flush_enable=flush_enable)
        self._pack_buffer = []
        self._sample_interval = decimal.Decimal(0)
        self._timestamp = decimal.Decimal(0)
        self._send_timestamp = False

    def reset(self):
        self._pack_buffer = []

    def sample(self, values):
        # Append to existing buffer
        self._pack_buffer = self._pack_buffer + list(values)

        # Check if output will fit perfectly into bytes
        input_len_remainder = (len(self._pack_buffer) % 8)
        if input_len_remainder == 0:
            byte_inputs = bytearray(self._pack_buffer)
            self._pack_buffer = []
        else:
            byte_inputs = bytearray(
                self._pack_buffer[
                    :len(self._pack_buffer) - input_len_remainder
                ])
            # Store bits that have not been output
            self._pack_buffer = self._pack_buffer[
                len(self._pack_buffer) - input_len_remainder:]
        if self.msb_first:
            input_data_bits = np.packbits(byte_inputs)
        else:
            input_data_bits = np.packbits(byte_inputs)
            input_data_bits = np.unpackbits(
                input_data_bits).reshape(-1, 8)[:, ::-1].ravel()
            input_data_bits = np.packbits(input_data_bits)

        output = list(input_data_bits)

        if self._send_timestamp and (len(output) > 0):
            # Send remainder of data word
            messages = [{"opcode": "sample", "data": [output[0]]}]
            # Add in timestamp
            messages.append({"opcode": "time", "data": self._timestamp})
            # Send rest of data words
            output.pop(0)
            messages += [{"opcode": "sample", "data": [output_bit]}
                         for output_bit in output]
            # Clear the timestamp flag
            self._send_timestamp = False
            return self.output_formatter(messages)
        else:
            return self.output_formatter(
                [{"opcode": "sample", "data": [output_bit]}
                 for output_bit in output])

    def flush_buffer(self, next_opcode):
        bits_in_buffer = len(self._pack_buffer)
        if bits_in_buffer > 0:
            flush_data = [0] * (8 - bits_in_buffer)
            result = self.sample(flush_data).output
            self._send_timestamp = False
            return result + [next_opcode]
        else:
            return [next_opcode]

    def clear_buffer(self, next_opcode):
        self._pack_buffer = []
        self._send_timestamp = False
        return [next_opcode]

    def non_stream(self, opcode):
        if self.flush_enable:
            return self.flush_buffer(opcode)
        else:
            return self.clear_buffer(opcode)

    def flush(self, *inputs):
        return self.output_formatter(
            self.flush_buffer({"opcode": "flush", "data": None}))

    def discontinuity(self, *inputs):
        return self.output_formatter(
            self.clear_buffer({"opcode": "discontinuity", "data": None}))

    def sample_interval(self, value):
        self._sample_interval = value
        return self.output_formatter(
            self.non_stream({"opcode": "sample_interval", "data": value}))

    def metadata(self, value):
        return self.output_formatter(
            self.non_stream({"opcode": "metadata", "data": value}))

    def time(self, value):
        if len(self._pack_buffer) == 0:
            return self.output_formatter([{"opcode": "time", "data": value}])
        else:
            self._timestamp = decimal.Decimal((8 - len(self._pack_buffer))
                                              * self._sample_interval) + value
            self._send_timestamp = True
            return self.output_formatter([])
