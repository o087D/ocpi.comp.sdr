#!/usr/bin/env python3

# Generates the input binary file for data_pack_b_uc testing.
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.


import os
import random

import ocpi_protocols
import ocpi_testing


class CustomGenerator(ocpi_testing.generator.BooleanGenerator):
    def custom(self, seed, subcase):
        # This test checks opcodes are handled correctly when they arrive in
        # the middle of a word that is being packed.
        random.seed(seed)
        # Back up original message length
        original_message_length = self.SAMPLE_DATA_LENGTH
        self.SAMPLE_DATA_LENGTH = random.randint(1, self.SAMPLE_DATA_LENGTH)
        # Make sure message length is not a multiple of 8
        if (self.SAMPLE_DATA_LENGTH % 8) == 0:
            self.SAMPLE_DATA_LENGTH += 1

        # Handle the different opcode types
        if subcase == "time":
            messages = [{"opcode": "sample_interval", "data": 1000000}]
            messages += self.time(seed, "positive")
        elif subcase == "sample_interval":
            messages = self.sample_interval(seed, "positive")
        elif subcase == "flush":
            messages = self.flush(seed, "single")
        elif subcase == "discontinuity":
            messages = self.discontinuity(seed, "single")
        elif subcase == "metadata":
            messages = self.metadata(seed, "positive")
        else:
            raise ValueError(
                f"Unexpected subcase of {subcase} for custom()")

        # Reset STREAM_DATA_LENGTH back to its original value
        self.SAMPLE_DATA_LENGTH = original_message_length

        return messages


arguments = ocpi_testing.get_generate_arguments()

subcase = os.environ["OCPI_TEST_subcase"]
msb_first = os.environ.get("OCPI_TEST_msb_first").lower() == "true"
flush_enable = (
    os.environ.get("OCPI_TEST_flush_on_non_stream_opcode").lower() == "true")

seed = ocpi_testing.get_test_seed(
    arguments.case, subcase, msb_first, flush_enable)

generator = CustomGenerator()

# Generate the test data messages
messages = generator.generate(seed, arguments.case, subcase,
                              arguments.case_number, arguments.subcase_number)
with ocpi_protocols.WriteMessagesFile(arguments.save_path, "bool_timed_sample") as file_id:
    file_id.write_dict_messages(messages)
