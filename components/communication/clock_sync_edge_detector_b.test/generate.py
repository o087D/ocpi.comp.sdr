#!/usr/bin/env python3

# Generates the input binary file for clock_sync_edge_detector testing
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.


import os
import random

import ocpi_protocols
import ocpi_testing


class ClockEdgeGenerator(ocpi_testing.generator.BooleanGenerator):
    def _get_random_symbols(self, seed, clock_scaling):
        # Symbol rate specified by samples_per_symbol.
        number_of_symbols = int(
            self.SAMPLE_DATA_LENGTH / int(samples_per_symbol))
        assert number_of_symbols > 0, ("Number of symbols must be "
                                       "greater than 0")

        # Clock scaling sets the scaling in symbol rate compated to the ideal
        # Clock_scaling=1.1: the symbol rate is 10% higher than the ideal value
        # Clock_scaling=0.9: the symbol rate is 10% lower than the ideal value
        assert clock_scaling > 0.0, "Clock scaling must be greater than 0"

        random.seed(seed)

        test_symbols = [
            bool(random.randint(0, 2)) for x in range(0, number_of_symbols)]
        symbol_clock = 0.0
        # Pre allocate space to avoid dynamic resizing slowing down process.
        # 16384 is used as it is the maximum number of data values supported
        # by the clock edge detector.
        test_data = [None] * 16384
        test_data_length = 0

        for symbol in test_symbols:
            while symbol_clock < 1.0:
                symbol_clock = (
                    symbol_clock + ((1.0 / int(samples_per_symbol)) *
                                    clock_scaling))
                if test_data_length < len(test_data):
                    test_data[test_data_length] = symbol
                    test_data_length += 1
                else:
                    raise ValueError(f"Attempted to add more than the maximum "
                                     + "allowed data values. Check clock "
                                     + "scaling value.")

            symbol_clock -= 1.0

        return test_data[:test_data_length]

    def typical(self, seed, subcase):
        # Typical behaviour for clock sync edge detector is x repeated values,
        # where x is equal to the number of symbols per sample, so override
        # the default typical behaviour for this particular case.
        return [
            {"opcode": "sample", "data": self._get_random_symbols(seed, 1)}]

    def property(self, seed, subcase):
        # Typical behaviour for clock sync edge detector is x repeated values,
        # where x is equal to the number of symbols per sample, so override
        # the default typical behaviour for this particular case.
        return [
            {"opcode": "sample", "data": self._get_random_symbols(seed, 1)}]

    def soak(self, seed, subcase):
        # Typical behaviour for clock sync edge detector is x repeated values,
        # where x is equal to the number of symbols per sample, so override
        # the default typical behaviour for this particular case.
        return [
            {"opcode": "sample", "data": self._get_random_symbols(seed, 1)}]

    def custom(self, seed, subcase):
        # Typical behaviour for clock sync edge detector is x repeated values,
        # where x is equal to the number of symbols per sample, so override
        # the default typical behaviour for this particular case.
        if subcase == "slower":
            return [
                {"opcode": "sample", "data": self._get_random_symbols(seed,
                                                                      0.9)}]
        elif subcase == "faster":
            return [
                {"opcode": "sample", "data": self._get_random_symbols(seed,
                                                                      1.1)}]
        elif subcase == "timestamp":
            # This test checks timestamps are advanced correctly
            # Back up original message length
            original_message_length = self.SAMPLE_DATA_LENGTH
            random.seed(seed)
            # Set sample interval to a non-zero value
            messages = [{"opcode": "sample_interval",
                         "data": random.randint(1, 100000000)}]
            for val in range(10):
                self.SAMPLE_DATA_LENGTH = random.randint(
                    1, original_message_length)
                messages += [
                    {"opcode": "time",
                     "data": random.randint(1, 100000000)}]
                messages += [
                    {"opcode": "sample",
                     "data": self._get_random_symbols(seed + val, 1)}]

            # Reset SAMPLE_DATA_LENGTH back to its original value
            self.SAMPLE_DATA_LENGTH = original_message_length
            return messages
        else:
            raise ValueError(f"Unexpected subcase of {subcase} for custom()")


arguments = ocpi_testing.get_generate_arguments()

subcase = os.environ["OCPI_TEST_subcase"]
samples_per_symbol = int(os.environ["OCPI_TEST_samples_per_symbol"])
sample_point = int(os.environ["OCPI_TEST_sample_point"])

seed = ocpi_testing.get_test_seed(
    arguments.case, subcase, samples_per_symbol, sample_point)

generator = ClockEdgeGenerator()

# Generate the test data messages
messages = generator.generate(seed, arguments.case, subcase,
                              arguments.case_number, arguments.subcase_number)

with ocpi_protocols.WriteMessagesFile(arguments.save_path, "bool_timed_sample") as file_id:
    file_id.write_dict_messages(messages)
