.. data_unpack_uc_b documentation

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


.. _data_unpack_uc_b:


Data unpacker (``data_unpack_uc_b``)
====================================
Unpacks a byte in to eight individual bits.

Design
------
This component takes in a stream of unsigned character inputs and serially outputs a stream of eight boolean outputs for each character input. The component can either output the most significant bit of the unsigned character first or the least significant bit first.

Interface
---------
.. literalinclude:: ../specs/data_unpack_uc_b-spec.xml
   :language: xml
   :lines: 1,19-

Ports
~~~~~
.. ocpi_documentation_ports::

   input: Primary input samples port.
   output: Primary output samples port.

Opcode handling
~~~~~~~~~~~~~~~
The data unpack operation is applied on data with sample opcodes only. Output stream messages over 16384 symbols long are split, otherwise message boundaries are preserved. All other opcodes pass through this component without any effect.

Properties
~~~~~~~~~~
.. ocpi_documentation_properties::

Parameters
~~~~~~~~~~
.. ocpi_documentation_properties::
   :parameters:

Implementations
---------------
.. ocpi_documentation_implementations:: ../data_unpack_uc_b.hdl ../data_unpack_uc_b.rcc

Example application
-------------------
.. literalinclude:: example_app.xml
   :language: xml
   :lines: 1,19-

Dependencies
------------
The dependencies to other elements in OpenCPI are:

 * :ref:`Message length limiter primitive <message_length_limiter-primitive>`

There is also a dependency on:

 * ``ieee.std_logic_1164``

 * ``ieee.numeric_std``

Limitations
-----------
Limitations of ``data_unpack_uc_b`` are:

 * None.

Testing
-------
.. ocpi_documentation_test_result_summary::
