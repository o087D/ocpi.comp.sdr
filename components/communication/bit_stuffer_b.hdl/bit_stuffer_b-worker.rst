.. bit_stuffer_b HDL worker

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


:orphan:

.. _bit_stuffer_b-HDL-worker:


``bit_stuffer_b`` HDL worker
============================

Detail
------
.. Figures would usually be referenced using :numref: however this only works
   when the page is part of a "toctree" (table of contents) since this is an
   orphan page it not included in a toctree and so :numref: cannot be used
   without getting an error.

A block diagram of the HDL worker implementation is shown below, where :math:`x[n]` is the input and :math:`y[n]` is the bit stuffed output.

.. figure:: bit_stuffer_b_hdl.svg
   :alt: Arrangement of flush inserter, bit stuffer and message limiter in HDL implementation.
   :align: center

   Bit stuffer HDL implementation.

The purpose of the flush inserter is to apply back pressure to the input interface and output ``flush_length`` zero samples whenever a flush opcode is received. The message limiter ensures the message length does not exceed the maximum message size allowed by the protocol.

.. ocpi_documentation_worker::

Utilisation
-----------
.. ocpi_documentation_utilisation::
