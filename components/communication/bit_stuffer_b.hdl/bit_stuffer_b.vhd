-- bit_stuffer_b HDL implementation.
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

library ieee;
use ieee.std_logic_1164.all;
library ocpi;
use ocpi.types.all;
library sdr_communication;
use sdr_communication.sdr_communication.bit_stuffer;

architecture rtl of bit_stuffer_b_worker is

  constant consecutive_ones_c : integer := to_integer(consecutive_ones);

  signal input_interface : worker_input_in_t;
  signal take            : std_logic;
  signal get_next_value  : std_logic;
  signal output_ready    : std_logic;
  signal valid_data      : std_logic;
  signal discontinuity   : std_logic;
  signal eom_d           : std_logic;
  signal reset           : std_logic;
  signal all_ones        : std_logic;
  signal bit_stuff       : std_logic;
  signal data_eom        : std_logic;
  signal output          : worker_output_out_t;

begin

  -- Insert flush directly into input data stream.
  -- input_interface is used instead of input_in in rest of component.
  -- Take signal is from when input_interface.ready = '1' and output port ready
  flush_insert_i : entity work.boolean_flush_injector
    generic map (
      DATA_IN_WIDTH_G      => input_in.data'length,
      MAX_MESSAGE_LENGTH_G => to_integer(OCPI_MAX_BYTES_OUTPUT)
      )
    port map (
      clk             => ctl_in.clk,
      reset           => ctl_in.reset,
      take_in         => take,
      input_in        => input_in,
      flush_length    => props_in.flush_length,
      input_out       => input_out,
      input_interface => input_interface
      );

  -- Take data when input and output are ready
  take <= output_ready and input_interface.ready and get_next_value;

  -- Signal when output is ready
  output_ready <= ctl_in.is_operating and output_in.ready;

  -- Signal for valid input data
  valid_data <= '1' when take = '1' and input_interface.valid = '1' and
                input_interface.opcode = bool_timed_sample_sample_op_e else '0';

  -- Signal for discontinuity opcode
  discontinuity <= '1' when take = '1' and
                   input_interface.opcode = bool_timed_sample_discontinuity_op_e else '0';

  -- Register eom flag for stream data
  eom_flag_p : process(ctl_in.clk)
  begin
    if rising_edge(ctl_in.clk) then
      if ctl_in.reset = '1' then
        eom_d <= '0';
      elsif valid_data = '1' then
        eom_d <= input_interface.eom;
      end if;
    end if;
  end process;

  -- Bit stuffer reset signal
  reset <= ctl_in.reset or discontinuity;

  -- Bit stuffer implementation
  bit_stuffer_i : bit_stuffer
    generic map (
      consecutive_ones_g => consecutive_ones_c
      )
    port map (
      clk            => ctl_in.clk,
      rst            => reset,
      enable         => output_ready,
      ready          => get_next_value,
      all_ones       => all_ones,
      data_valid_in  => valid_data,
      data_in        => input_interface.data(0),
      data_valid_out => open,
      data_out       => open
      );

  -- When the last input was the EOM, hold the EOM flag until the last sample
  bit_stuff <= all_ones and valid_data and input_interface.data(0);
  data_eom  <= (not get_next_value and eom_d) or
              (get_next_value and input_interface.eom and not bit_stuff);

  -- Streaming interface output
  output_mux_p : process(take, valid_data, input_interface, output_ready,
                         get_next_value, data_eom)
  begin
    output.data(7 downto 1) <= input_interface.data(7 downto 1);
    if take = '1' then
      output.give        <= input_interface.ready;
      output.valid       <= input_interface.valid;
      output.som         <= input_interface.som;
      output.byte_enable <= input_interface.byte_enable;
      output.opcode      <= input_interface.opcode;
      output.data(0)     <= input_interface.data(0);
    else
      output.give        <= output_ready and not get_next_value;
      output.valid       <= '1';
      output.som         <= '0';
      output.byte_enable <= (others => '1');
      output.opcode      <= bool_timed_sample_sample_op_e;
      output.data(0)     <= '0';
    end if;
    if take = '1' and valid_data = '0' then
      output.eom <= input_interface.eom;
    else
      output.eom <= data_eom;
    end if;
  end process;

  -- Split stream messages over OCPI_MAX_BYTES_OUTPUT words, as this is the
  -- maximum size specified by the protocol.
  limit_message_size_i : entity work.boolean_protocol_message_limiter
    generic map(
      DATA_OUT_WIDTH_G     => output_out.data'length,
      MAX_MESSAGE_LENGTH_G => to_integer(OCPI_MAX_BYTES_OUTPUT)
      )
    port map(
      clk                => ctl_in.clk,
      reset              => ctl_in.reset,
      enable             => output_ready,
      output_out         => output,
      output_out_limited => output_out
      );

end rtl;
