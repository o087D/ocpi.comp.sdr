#!/usr/bin/env python3

# Python implementation of multiplicative descrambler block for verification
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.


import ocpi_testing


class MultiplicativeDescrambler(ocpi_testing.Implementation):
    def __init__(self, polynomial_taps, seed, flush_length):
        polynomial_taps = polynomial_taps.split(",")

        super().__init__(polynomial_taps=polynomial_taps, seed=seed,
                         flush_length=flush_length)
        self.reset()

    def reset(self):
        # Reset lfsr state to seed
        formatter = "{0:0" + self.polynomial_taps[0] + "b}"
        self.seed &= 2**int(self.polynomial_taps[0]) - 1
        self.state = [int(val) for val in list(formatter.format(self.seed))]

    def sample(self, data):
        output_values = []
        for value in data:
            # Compute the value to be xor'd with input
            xor_value = 0
            for n in reversed(range(len(self.polynomial_taps))):
                if int(self.polynomial_taps[n]):
                    xor_value ^= self.state[int(self.polynomial_taps[n]) - 1]
            # Shift the state
            for k in reversed(range(1, int(self.polynomial_taps[0]))):
                self.state[k] = self.state[k - 1]
            # Compute the output
            self.state[0] = value
            output_values.append(bool(xor_value ^ value))
        return self.output_formatter(
            [{"opcode": "sample", "data": output_values}])

    def discontinuity(self, values):
        self.reset()
        return self.output_formatter(
            [{"opcode": "discontinuity", "data": None}])

    def flush(self, *inputs):
        if self.flush_length > 0:
            flush_data = [0] * self.flush_length

            # As stream() will return a named tuple with the names of the
            # output port names, and MultiplicativeDescrambler only has one
            # output port called "output", get the messages from that port.
            # Then add the flush message as this will be passed through after
            # the flushed data.
            resulting_messages = self.sample(flush_data).output
            resulting_messages.append({"opcode": "flush", "data": None})

            return self.output_formatter(resulting_messages)
        else:
            return self.output_formatter([{"opcode": "flush", "data": None}])
