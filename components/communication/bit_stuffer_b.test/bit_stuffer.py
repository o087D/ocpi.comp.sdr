#!/usr/bin/env python3

# Python implementation of bit stuffer block for verification
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.


import ocpi_testing


class BitStuffer(ocpi_testing.Implementation):
    def __init__(self, consecutive_ones, flush_length):
        super().__init__(consecutive_ones=consecutive_ones,
                         flush_length=flush_length)
        self._max_message_samples = 16384
        self.all_ones = [True] * (self.consecutive_ones - 1)
        self.all_zeros = [False] * (self.consecutive_ones - 1)
        self.reset()

    def reset(self):
        self.input_buffer = self.all_zeros

    def sample(self, data):
        output_values = []
        for value in data:
            # Test whether to insert zero
            if (self.consecutive_ones == 1 or
                    self.input_buffer == self.all_ones) and value:
                output_values += [True, False]
                self.input_buffer = self.input_buffer[2:] + [True, False]
            else:
                output_values += [value]
                self.input_buffer = self.input_buffer[1:] + [value]

        # Sample messages cannot have more than 16384 samples in each message.
        # As this component increases the data size, break long messages up
        # into messages with no more than 16384 samples.
        messages = [{
            "opcode": "sample",
            "data": output_values[index:
                                  index + self._max_message_samples]}
                    for index in range(0, len(output_values),
                                       self._max_message_samples)]

        return self.output_formatter(messages)

    def discontinuity(self, values):
        self.reset()
        return self.output_formatter(
            [{"opcode": "discontinuity", "data": None}])

    def flush(self, *inputs):
        if self.flush_length > 0:
            flush_data = [False] * self.flush_length

            # As stream() will return a named tuple with the names of the
            # output port names, and BitStuffer only has one output port
            # called "output", get the messages from that port.
            # Then add the flush message as this will be passed through after
            # the flushed data.
            resulting_messages = self.sample(flush_data).output
            resulting_messages.append({"opcode": "flush", "data": None})

            return self.output_formatter(resulting_messages)
        else:
            return self.output_formatter([{"opcode": "flush", "data": None}])
