-- multiplicative_descrambler_b HDL implementation.
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;
library ocpi;
use ocpi.types.all;
library sdr_communication;
use sdr_communication.sdr_communication.multiplicative_descrambler;

architecture rtl of multiplicative_descrambler_b_worker is

  -- Function to convert polynomial taps into a std_logic_vector
  -- E.g. 4,2,1 -> "11101" (1 + z^-1 + z^-2 + z^-4)
  function taps_to_poly(taps : in uchar_array_t)
    return std_logic_vector is
    variable poly : std_logic_vector(to_integer(taps(0)) downto 0);
    variable tap  : std_logic_vector(to_integer(taps(0)) downto 0);
  begin
    poly := (others => '0');
    for i in 0 to taps'length - 1 loop
      tap := std_logic_vector(
        to_unsigned(1, tap'length) sll tap'length - 1 - to_integer(taps(i)));
      poly := poly or tap;
    end loop;
    tap  := std_logic_vector(to_unsigned(1, tap'length) sll tap'length - 1);
    poly := poly or tap;
    return poly;
  end function;

  constant poly_size_c  : integer          := to_integer(polynomial_taps(0));
  constant polynomial_c : std_logic_vector := taps_to_poly(polynomial_taps);

  signal take             : std_logic;
  signal valid_data       : std_logic;
  signal discontinuity    : std_logic;
  signal reset            : std_logic;
  signal seed             : std_logic_vector(poly_size_c - 1 downto 0);
  signal descrambled_data : std_logic;
  signal data_out         : std_logic;
  signal input_interface  : worker_input_in_t;

begin

  -- Insert flush directly into input data stream.
  -- input_interface is used instead of input_in in rest of component.
  -- Take signal is from when input_interface.ready = '1' and output port ready.
  flush_insert_i : entity work.boolean_flush_injector
    generic map (
      DATA_IN_WIDTH_G      => input_in.data'length,
      MAX_MESSAGE_LENGTH_G => to_integer(OCPI_MAX_BYTES_OUTPUT)
      )
    port map (
      clk             => ctl_in.clk,
      reset           => ctl_in.reset,
      take_in         => take,
      input_in        => input_in,
      flush_length    => props_in.flush_length,
      input_out       => input_out,
      input_interface => input_interface
      );

  -- Take data when input and output are ready
  take <= ctl_in.is_operating and input_interface.ready and output_in.ready;

  -- Signal for valid input data
  valid_data <= '1' when take = '1' and input_interface.valid = '1' and
                input_interface.opcode = bool_timed_sample_sample_op_e else '0';

  -- Signal for discontinuity opcode
  discontinuity <= '1' when take = '1' and
                   input_in.opcode = bool_timed_sample_discontinuity_op_e else '0';

  -- Convert seed into std_logic_vector
  seed <= std_logic_vector(resize(props_in.seed, poly_size_c));

  -- Descrambler reset signal
  reset <= ctl_in.reset or discontinuity or props_in.seed_written;

  -- Descrambler implementation
  multiplicative_descrambler_i : multiplicative_descrambler
    generic map (
      polynomial_g => polynomial_c
      )
    port map (
      clk            => ctl_in.clk,
      rst            => reset,
      seed           => seed,
      data_valid_in  => valid_data,
      data_in        => input_interface.data(0),
      data_valid_out => open,
      data_out       => descrambled_data
      );

  -- Output descrambled data
  data_out <= descrambled_data when valid_data = '1' else
              input_interface.data(0);

  -- Streaming interface output
  output_out.give        <= input_interface.ready;
  output_out.valid       <= input_interface.valid;
  output_out.som         <= input_interface.som;
  output_out.eom         <= input_interface.eom;
  output_out.data        <= input_interface.data(7 downto 1) & data_out;
  output_out.byte_enable <= input_interface.byte_enable;
  output_out.opcode      <= input_interface.opcode;

end rtl;
