// RCC implementation of abs_l_l worker.
//
// This file is protected by Copyright. Please refer to the COPYRIGHT file
// distributed with this source distribution.
//
// This file is part of OpenCPI <http://www.opencpi.org>
//
// OpenCPI is free software: you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
// more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include "symbol_mapper_uc_l-worker.hh"

using namespace OCPI::RCC;  // for easy access to RCC data types and constants
using namespace Symbol_mapper_uc_lWorkerTypes;

class Symbol_mapper_uc_lWorker : public Symbol_mapper_uc_lWorkerBase {
  const int32_t *m_values = 0;
  size_t sent_samples = 0;

  RCCResult start() {
    sent_samples = 0;
    return RCC_OK;
  }

  RCCResult values_written() {
    m_values = properties().values;
    return RCC_OK;
  }

  RCCResult run(bool) {
    if (input.opCode() == Uchar_timed_sampleSample_OPERATION) {
      size_t length = input.sample().data().size();
      const uint8_t *in_data = input.sample().data().data();
      int32_t *out_data = output.sample().data().data();

      output.setOpCode(Long_timed_sampleSample_OPERATION);
      size_t max_length =
          SYMBOL_MAPPER_UC_L_OCPI_MAX_BYTES_OUTPUT / sizeof(*out_data);
      in_data = &in_data[sent_samples];

      size_t unsent_samples = length - sent_samples;
      if (unsent_samples <= max_length) {
        output.sample().data().resize(unsent_samples);
        for (size_t i = 0; i < unsent_samples; i++) {
          int32_t symbol = m_values[*in_data];
          *out_data = symbol;
          in_data++;
          out_data++;
        }
        // All input data has been processed prepare for next input message
        sent_samples = 0;
        return RCC_ADVANCE;
      } else {
        output.sample().data().resize(max_length);
        for (size_t i = 0; i < max_length; i++) {
          int32_t symbol = m_values[*in_data];
          *out_data = symbol;
          in_data++;
          out_data++;
        }
        sent_samples = sent_samples + max_length;
        output.advance(max_length);
        return RCC_OK;
      }
    } else if (input.opCode() == Uchar_timed_sampleTime_OPERATION) {
      // Pass through time opcode and time data
      output.setOpCode(Long_timed_sampleTime_OPERATION);
      output.time().fraction() = input.time().fraction();
      output.time().seconds() = input.time().seconds();
      return RCC_ADVANCE;
    } else if (input.opCode() == Uchar_timed_sampleSample_interval_OPERATION) {
      // Pass through sample interval opcode and sample interval data
      output.setOpCode(Long_timed_sampleSample_interval_OPERATION);
      output.sample_interval().fraction() = input.sample_interval().fraction();
      output.sample_interval().seconds() = input.sample_interval().seconds();
      return RCC_ADVANCE;
    } else if (input.opCode() == Uchar_timed_sampleFlush_OPERATION) {
      output.setOpCode(Long_timed_sampleFlush_OPERATION);
      return RCC_ADVANCE;
    } else if (input.opCode() == Uchar_timed_sampleDiscontinuity_OPERATION) {
      output.setOpCode(Long_timed_sampleDiscontinuity_OPERATION);
      return RCC_ADVANCE;
    } else if (input.opCode() == Uchar_timed_sampleMetadata_OPERATION) {
      // Pass through metadata opcode, id, and data
      output.setOpCode(Long_timed_sampleMetadata_OPERATION);
      output.metadata().id() = input.metadata().id();
      output.metadata().value() = input.metadata().value();
      return RCC_ADVANCE;
    } else {
      setError("Unknown OpCode Received");
      return RCC_FATAL;
    }
  }
};

SYMBOL_MAPPER_UC_L_START_INFO
// Insert any static info assignments here (memSize, memSizes, portInfo)
// e.g.: info.memSize = sizeof(MyMemoryStruct);
SYMBOL_MAPPER_UC_L_END_INFO
