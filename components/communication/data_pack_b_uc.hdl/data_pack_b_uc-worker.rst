.. data_pack_b_uc HDL worker

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


:orphan:

.. _data_pack_b_uc-HDL-worker:


``data_pack_b_uc`` HDL worker
=============================

Detail
------
The data packer uses a state machine to handle shifting and outputting data. This ensures that non-sample opcodes are correctly handled by the component.

The component has a larger footprint than a typical data packer due to its correct handling of interface edge cases.

The component only applies backpressure on the input interface when a non-sample message is received in the middle of a byte that is being packed.

The component has a latency of one clock cycle for all sample messages.

.. ocpi_documentation_worker::

Utilisation
-----------
.. ocpi_documentation_utilisation::
