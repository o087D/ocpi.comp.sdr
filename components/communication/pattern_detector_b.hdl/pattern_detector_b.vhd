-- VHDL implementation of pattern detector worker.
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

-- Searches a boolean stream for a user-defined pattern of up to
--   64 bits in length.
-- Injects a discontinuity opcode if the pattern is detected.
-- Uses a user-defined bit mask to select which bits are compared.

library IEEE;
use IEEE.std_logic_1164.all;
use ieee.numeric_std.all;
library ocpi;
use ocpi.types.all;
architecture rtl of pattern_detector_b_worker is

  constant delay_c : integer := 1;

  type state_t is (passthrough, discontinuity_inject);

  signal take           : std_logic;
  signal give           : std_logic;
  signal flush          : std_logic;
  signal discontinuity  : std_logic;
  signal enable         : std_logic;
  signal reset          : std_logic;
  signal input_take     : std_logic;
  signal match          : std_logic;
  signal add_som        : std_logic;
  signal current_state  : state_t;
  signal inject_discont : std_logic;
  signal result         : std_logic_vector(63 downto 0) := (others => '0');
  signal shift_register : std_logic_vector(63 downto 0) := (others => '0');
  signal new_register   : std_logic_vector(63 downto 0) := (others => '0');
  signal mask           : std_logic_vector(63 downto 0) := (others => '0');
  signal pattern        : std_logic_vector(63 downto 0) := (others => '0');
  signal interface_in   : worker_input_in_t;

begin

  enable <= ctl_in.is_operating and output_in.ready;

  -- Signals for incoming flush and discontinuity handling
  flush         <= '1' when input_in.opcode = bool_timed_sample_flush_op_e         else '0';
  discontinuity <= '1' when input_in.opcode = bool_timed_sample_discontinuity_op_e else '0';
  reset         <= '1' when ctl_in.reset = '1'                                     else '0';

  -- Don't take while injecting discontinuity
  take           <= input_in.ready and enable and not inject_discont;
  input_out.take <= take;

  inject_discont <= '1' when current_state = discontinuity_inject else '0';

  -- Save properties as vectors
  mask    <= std_logic_vector(props_in.bitmask);
  pattern <= std_logic_vector(props_in.pattern);
  -- Interface delay module
  -- Delays streaming interface signals to align with the delay introduced by
  -- the pattern detection operation.
  interface_delay_i : entity work.boolean_protocol_delay
    generic map (
      DELAY_G => delay_c
      )
    port map (
      clk                 => ctl_in.clk,
      reset               => ctl_in.reset,
      enable              => enable,
      take_in             => input_take,
      input_in            => interface_in,
      processed_stream_in => shift_register(0),
      output_out          => output_out
      );

  state_handler : process(ctl_in.clk)
  begin
    if rising_edge(ctl_in.clk) then
      if reset = '1' then
        current_state <= passthrough;
      elsif enable = '1' then
        case current_state is
          when passthrough =>
            add_som <= '0';
            -- Match calculated async, so we know imediately when
            -- there is a match and we handle it on next clock cycle
            if match = '1' and input_in.valid = '1' and input_in.ready = '1' and input_in.opcode = bool_timed_sample_sample_op_e then
              current_state <= discontinuity_inject;
            end if;
          when discontinuity_inject =>
            -- Discontinuity only takes 1 cycle
            current_state <= passthrough;
            add_som       <= '1';
          when others =>
            current_state <= passthrough;
            add_som       <= '0';
        end case;
      end if;
    end if;
  end process;

  -- Slip in a discontinuity
  discontinuity_injector : process(take, current_state, input_in, add_som, match)
  begin
    interface_in.data <= input_in.data;

    if current_state = passthrough then
      interface_in.som         <= input_in.som or add_som;
      interface_in.eom         <= input_in.eom or match;
      interface_in.valid       <= input_in.valid;
      interface_in.byte_enable <= input_in.byte_enable;
      interface_in.opcode      <= input_in.opcode;
      -- Remove incoming discontinuities
      if input_in.opcode = bool_timed_sample_discontinuity_op_e then
        input_take <= '0';
      -- Remove late EOM if immediately after match
      elsif input_in.opcode = bool_timed_sample_sample_op_e
        and input_in.valid = '0' and input_in.som = '0' and input_in.eom = '1'
        and add_som = '1' then
        input_take <= '0';
      else
        input_take <= take;
      end if;

    else
      -- Insert discontinuity opcode
      interface_in.som         <= '1';
      interface_in.eom         <= '1';
      interface_in.valid       <= '0';
      interface_in.byte_enable <= (others => '0');
      interface_in.opcode      <= bool_timed_sample_discontinuity_op_e;
      input_take               <= '1';
    end if;
  end process;

  memory : process(ctl_in.clk)
  begin
    if rising_edge(ctl_in.clk) then
      if discontinuity = '1' or flush = '1' or reset = '1' then
        shift_register <= (others => '0');
      elsif (input_in.valid = '1' and input_in.ready = '1' and take = '1') then
        shift_register <= new_register;
      end if;
    end if;
  end process;

  -- Check for match asynchronously; if mask is all zeros a match can never
  -- occur
  new_register <= shift_register(shift_register'high - 1 downto 0) & input_in.data(0);
  result       <= (mask and new_register) xnor (mask and pattern);
  match        <= '1' when result = X"FFFFFFFFFFFFFFFF" and mask /= X"0000000000000000" else '0';

end rtl;
