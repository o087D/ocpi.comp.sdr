.. counter_b_uc documentation

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


.. _counter_b_uc:


Counter (``counter_b_uc``)
==========================
Counts the number of times a ``true`` data value occurs on the input.

Design
------
Counter that increments or decrements by one each time it receives a boolean ``true``. When a ``false`` is received the counter value does not change. Whether the counter increments or decrements is controlled by the ``direction`` property.

Interface
---------
.. literalinclude:: ../specs/counter_b_uc-spec.xml
   :language: xml
   :lines: 1,19-

Ports
~~~~~
.. ocpi_documentation_ports::

   input: Primary input samples port.
   output: Primary output samples port.

Opcode handling
~~~~~~~~~~~~~~~
Only data values within a sample opcode message are counted.

Discontinuity and flush opcode messages reset the counter value to zero or ``size``, depending on the mode of operation, and are then output of the component. All other opcodes pass through this component without any effect.

Properties
~~~~~~~~~~
.. ocpi_documentation_properties::

Parameters
~~~~~~~~~~
.. ocpi_documentation_properties::
   :parameters:

Implementations
---------------
.. ocpi_documentation_implementations:: ../counter_b_uc.hdl ../counter_b_uc.rcc

Example application
-------------------
.. literalinclude:: example_app.xml
   :language: xml
   :lines: 1,19-

Dependencies
------------
The dependencies to other elements in OpenCPI are:

 * :ref:`Counter up-down primitive <counter_updown-primitive>`

 * :ref:`Protocol interface delay primitive <protocol_interface_delay-primitive>`

There is also a dependency on:

 * ``ieee.std_logic_1164``

 * ``ieee.numeric_std``

Limitations
-----------
Limitations of ``counter_b_uc`` are:

 * Due to the size of an unsigned character the maximum output value is 255, after this it will roll-over to 0.

 * Due to the output being an unsigned character the minimum output value is 0, after this it will reset to the value ``size`` - 1.

 * When in increment mode, if a value greater than ``size`` - 1 is loaded into the component this will roll-over to 0.

 * When in decrement mode, if a value greater than ``size`` - 1 is loaded into the component then this the used value will be ``size`` - 1.

 * To use the maximum value of ``size`` it must be set to 0.

Testing
-------
.. ocpi_documentation_test_result_summary::
