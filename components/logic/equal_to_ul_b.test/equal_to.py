#!/usr/bin/env python3

# Python implementation of equal to block for verification
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.


import ocpi_testing


class Equal_To(ocpi_testing.Implementation):
    def __init__(self, value, invert_output):
        super().__init__(value=value, invert_output=invert_output)

        self._below_value = self.invert_output
        self._above_value = not(self.invert_output)

    def reset(self):
        pass

    def sample(self, values):
        output_values = [self._below_value] * len(values)
        for index, value in enumerate(values):
            if value == self.value:
                output_values[index] = self._above_value

        return self.output_formatter(
            [{"opcode": "sample", "data": output_values}])
