.. carrier_mixer_xs HDL worker

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


:orphan:

.. _carrier_mixer_xs-HDL-worker:


``carrier_mixer_xs`` HDL worker
===============================
HDL implementation using a numerically controlled oscillator (NCO) for the carrier generator, which used the CORDIC algorithm to generate an approximation of sine and cosine.

Detail
------
.. ocpi_documentation_worker::

Utilisation
-----------
.. ocpi_documentation_utilisation::
