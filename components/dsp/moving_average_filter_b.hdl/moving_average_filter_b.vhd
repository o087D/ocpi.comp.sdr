-- HDL Implementation of moving average filter.
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

-- Averages '0' and '1' over a settable averaging window.
-- Averaging window should be set to a odd number for best results.
-- If over half the window is filled with '1's then the output is a '1'
--   otherwise the output is '0'.

-- Input is the LSB of the input word.
-- Output is the LSB out the output word.

-- Data is only taken from the input when both the output and input are ready.
-- Output is one clock cycle after the input (assuming the output is ready).
-- This module fully supports back pressure.
-- Zero length messages are passed through.

library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;
use ieee.math_real.all;
library ocpi;
use ocpi.types.all;

architecture rtl of moving_average_filter_b_worker is

  -- "ones_counter" needs to be big enough to hold the number of elements in the
  -- averaging window.
  -- I.e. if the window is 63 bits then we need 6 bits to hold the number 63
  constant taps_c              : integer := to_integer(maximum_moving_average_size);
  constant ones_counter_size_c : integer := integer(ceil(log2(real(taps_c))));
  constant delay_c             : integer := 1;

  signal take                     : std_logic;
  signal valid_data               : std_logic;
  signal output_ready             : std_logic;
  signal input_register           : std_logic_vector(taps_c - 1 downto 0);
  signal data                     : std_logic_vector(input_in.data'high downto 0);
  signal new_bit                  : std_logic;
  signal oldest_bit               : std_logic;
  signal oldest_bit_pointer       : natural range 0 to taps_c - 1;
  signal ones_counter             : unsigned(ones_counter_size_c - 1 downto 0);
  signal moving_average_size      : unsigned(ones_counter_size_c - 1 downto 0);
  signal half_moving_average_size : unsigned(ones_counter_size_c - 1 downto 0);
  signal input_interface          : worker_input_in_t;

begin

  ------------------------------------------------------------------------------
  -- Flush inject
  ------------------------------------------------------------------------------
  -- Insert flush directly into input data stream.
  -- input_interface is used instead of input_in in rest of component.
  -- Take signal is from when input_interface.ready = '1' and output port ready.
  flush_insert_i : entity work.boolean_flush_injector
    generic map (
      DATA_IN_WIDTH_G      => input_in.data'length,
      MAX_MESSAGE_LENGTH_G => to_integer(OCPI_MAX_BYTES_OUTPUT)
      )
    port map (
      clk             => ctl_in.clk,
      reset           => ctl_in.reset,
      take_in         => take,
      input_in        => input_in,
      flush_length    => props_in.flush_length,
      input_out       => input_out,
      input_interface => input_interface
      );
  -- Clock enable signal
  output_ready <= ctl_in.is_operating and output_in.ready;

  -- Take data when input and output are ready
  take <= input_interface.ready and output_ready;

  -- Signal for valid input data
  valid_data <= '1' when take = '1' and input_interface.valid = '1' and
                input_interface.opcode = bool_timed_sample_sample_op_e else '0';

  -- Only take the bits required from the moving_average_size property to
  -- prevent addressing part of the input_register that don't exist
  moving_average_size <= props_in.moving_average_size(ones_counter_size_c - 1 downto 0);

  -- Calculate floor(moving_average_size / 2)
  half_moving_average_size <= '0' & moving_average_size(moving_average_size'length - 1 downto 1);

  -- Interface delay module
  -- Delays streaming interface signals to align with the delay introduced by
  -- the moving average calculation.
  interface_delay_i : entity work.boolean_protocol_delay
    generic map (
      DELAY_G              => delay_c,
      DATA_IN_WIDTH_G      => input_in.data'length,
      PROCESSED_DATA_MUX_G => '1'
      )
    port map (
      clk                 => ctl_in.clk,
      reset               => ctl_in.reset,
      enable              => output_ready,
      take_in             => take,
      input_in            => input_interface,
      processed_stream_in => data,
      output_out          => output_out
      );

  -- Set up signals connected to the oldest and newest bits in the averaging
  -- window
  oldest_bit_pointer <= to_integer(moving_average_size - 1);
  oldest_bit         <= input_register(oldest_bit_pointer);
  new_bit            <= input_interface.data(0);

  -- Implement moving average
  moving_average_p : process(ctl_in.clk)
  begin
    if rising_edge(ctl_in.clk) then
      if ctl_in.reset = '1' then
        input_register <= (others => '0');
        ones_counter   <= (others => '0');
      elsif valid_data = '1' then
        -- Shift data in from the LSB of the vector and out on MSB
        input_register <= input_register(input_register'length - 2 downto 0) & new_bit;
        -- Update the count of '1's in the averaging window
        -- Keeping track of the input and output of the averaging window
        -- prevents having to add up a large number of 1 bit numbers in a
        -- single clock cycle
        if oldest_bit = '1' and new_bit = '0' then
          ones_counter <= ones_counter - 1;
        elsif oldest_bit = '0' and new_bit = '1' then
          ones_counter <= ones_counter + 1;
        end if;
      end if;
    end if;
  end process;

  -- Moving average result
  data <= "00000001" when ones_counter > half_moving_average_size else "00000000";

end rtl;
