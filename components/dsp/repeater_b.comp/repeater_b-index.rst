.. repeater_b documentation

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


.. _repeater_b:


Repeater (``repeater_b``)
=========================
Repeats each input data value a settable number of times.

Design
------
The mathematical representation of the implementation is given in :eq:`repeater_b-equation`.

.. math::
   :label: repeater_b-equation

   y[n] = x\left [ \left \lfloor \frac{n}{N+1} \right \rfloor \right ]

In :eq:`repeater_b-equation`:

 * :math:`x[n]` is the input values.

 * :math:`y[n]` is the output values.

 * :math:`N` is the number of times a sample is to be repeated.

When :math:`N` is zero the input data is passed through unmodified, when :math:`N` is one each input sample will be output twice (once repeated).

Interface
---------
.. literalinclude:: ../specs/repeater_b-spec.xml
   :language: xml
   :lines: 1,19-

Ports
~~~~~
.. ocpi_documentation_ports::

   input: Primary input samples port.
   output: Primary output samples port.

Opcode handling
~~~~~~~~~~~~~~~
Only values in a sample opcode message are repeated. Message boundaries will be kept in the same relative position to the data, unless the output message would exceed the maximum message length for the protocol type, in which case the message is split.

All other opcode messages pass through this component without any effect.

Properties
~~~~~~~~~~
.. ocpi_documentation_properties::

   samples: When set to zero the input data samples are passed through unmodified.

Parameters
~~~~~~~~~~
.. ocpi_documentation_properties::
   :parameters:

Implementations
---------------
.. ocpi_documentation_implementations:: ../repeater_b.hdl

Example application
-------------------
.. literalinclude:: example_app.xml
   :language: xml
   :lines: 1,19-

Dependencies
------------
The dependencies to other elements in OpenCPI are:

 * :ref:`Message length limiter primitive <message_length_limiter-primitive>`

There is also a dependency on:

 * ``ieee.std_logic_1164``

 * ``ieee.numeric_std``

Limitations
-----------
Limitations of ``repeater_b`` are:

 * None.

Testing
-------
.. ocpi_documentation_test_result_summary::
