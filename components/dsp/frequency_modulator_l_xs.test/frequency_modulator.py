#!/usr/bin/env python3

# Python implementation of frequency_modulator block for verification
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

import numpy as np

import ocpi_testing


class FrequencyModulator(ocpi_testing.Implementation):
    def __init__(self, modulator_amplitude):
        super().__init__(modulator_amplitude=modulator_amplitude)
        self.phase = 0

    def reset(self):
        self.phase = 0
        pass

    def sample(self, values):
        output_length = len(values)
        output = [0] * output_length
        for i in range(0, output_length):
            complex_val = 1.646760258 * \
                self.modulator_amplitude * np.exp(complex(0, self.phase))
            output[i] = complex(round(complex_val.real),
                                round(complex_val.imag))
            delta = 2.0 * np.pi * values[i] / 2**32
            self.phase = (self.phase + delta) % (2.0 * np.pi)
        return self.output_formatter(
            [{"opcode": "sample", "data": output}])
