#!/usr/bin/env python3

# Python implementation of scaled FIR filter for testing
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

import ocpi_testing
import numpy
import scipy.signal


class FirFilterScaled(ocpi_testing.Implementation):
    def __init__(self, taps, scale_factor, flush_length):
        super().__init__(taps=taps, scale_factor=scale_factor,
                         flush_length=flush_length)

        self._filter_buffer = [0] * len(self.taps)
        self._max_message_samples = 8192

    def reset(self):
        self._filter_buffer = [0] * len(self.taps)

    def sample(self, input_):
        output = self._filter(input_)
        messages = [{
            "opcode": "sample",
            "data": output[index: index + self._max_message_samples]}
            for index in range(0, len(output),
                               self._max_message_samples)]
        return self.output_formatter(messages)

    def flush(self, input_):
        if self.flush_length > 0:
            flush_data = [0] * self.flush_length
            messages = self.sample(flush_data).output
            messages.append({"opcode": "flush", "data": None})
            return self.output_formatter(messages)
        else:
            return self.output_formatter([
                {"opcode": "flush", "data": None}])

    def _filter(self, values):
        output = [0] * len(values)
        for index, value in enumerate(values):
            # Append value to end since scipy.signal.lfilter() does a "direct
            # II transpose" implementation, meaning tap "0" is multiplied by
            # value "N", tap "1" is multiplied by value "N-1", etc.
            self._filter_buffer = scipy.append(self._filter_buffer, value)[1:]
            filter_result = int(scipy.signal.lfilter(self.taps, 1.0,
                                                     self._filter_buffer)[-1])
            if self.scale_factor == 0:
                output[index] = numpy.int16(filter_result)
            else:
                shifted_result = numpy.right_shift(
                    filter_result, self.scale_factor - 1) + 1
                output[index] = numpy.int16(
                    numpy.right_shift(shifted_result, 1))
        return output
