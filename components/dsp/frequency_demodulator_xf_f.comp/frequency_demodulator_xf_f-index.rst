.. frequency_demodulator_xf_f documentation

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


.. meta::
   :keywords: atan2


.. _frequency_demodulator_xf_f:


Frequency demodulator (``frequency_demodulator_xf_f``)
======================================================
Finds the difference in angle between two consecutive complex values, so the phase rotation between points is found to demodulate a signal where the information is encoded in the waveform's frequency.

Design
------
The angle between consecutive complex values is found using the ``atan2`` operation, which ensures the correct sign (positive or negative) as well as angle magnitude is returned for the quadrant the input value is in.

The mathematical representation of the implementation is given in :eq:`frequency_demodulator_xf_f-equation`.

.. math::
   :label: frequency_demodulator_xf_f-equation

   \theta[n] = \tan^{-1} \left( \frac{ Re\{x[n-1]\} Im\{x[n]\} - Im\{x[n-1]\} Re\{x[n]\} }{ Re\{x[n-1]\} Re\{x[n]\} + Im\{x[n-1]\} Im\{x[n]\} } \right)

In :eq:`frequency_demodulator_xf_f-equation`:

 * :math:`x[n]` is the input values.

 * :math:`\theta[n]` is the angle between points and the output of the component.

 * :math:`Re\{x[n]\}` is the real part of :math:`x[n]` and :math:`Im\{x[n]\}` is the imaginary part of :math:`x[n]`.

A illustration of the angle found by this component is shown in :numref:`frequency_demodulator_xf_f-diagram`.

.. _frequency_demodulator_xf_f-diagram:

.. figure:: frequency_demodulator_xf_f.svg
   :alt: Illustration of the angle the frequency demodulation component returns.
   :align: center

   Angle returned by frequency demodulation is :math:`\theta`.

For output :math:`\theta[n]` the angle is between points :math:`x[n]` and :math:`x[n-1]`.

Interface
---------
.. literalinclude:: ../specs/frequency_demodulator_xf_f-spec.xml
   :language: xml
   :lines: 1,19-

Ports
~~~~~
.. ocpi_documentation_ports::

   input: Primary input samples port.
   output: Primary output samples port.

Opcode handling
~~~~~~~~~~~~~~~
The frequency demodulation is only calculated for values in a sample opcode message.

All other opcodes pass through this component without any effect.

Properties
~~~~~~~~~~
.. ocpi_documentation_properties::

Parameters
~~~~~~~~~~
.. ocpi_documentation_properties::
   :parameters:

Implementations
---------------
.. ocpi_documentation_implementations:: ../frequency_demodulator_xf_f.rcc

Example application
-------------------
.. literalinclude:: example_app.xml
   :language: xml
   :lines: 1,19-

Dependencies
------------
The dependencies to other elements in OpenCPI are:

 * None.

There is also a dependency on:

 * ``cmath``

Limitations
-----------
Limitations of ``frequency_demodulator_xf_f`` are:

 * None.

Testing
-------
.. ocpi_documentation_test_result_summary::
