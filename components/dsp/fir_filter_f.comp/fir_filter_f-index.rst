.. fir_filter_f documentation

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


.. _fir_filter_f:


Finite impulse response (FIR) filter (``fir_filter_f``)
=======================================================
Applies finite impulse response (FIR) filter to a stream of values.

Design
------
The mathematical representation of the implementation is given in :eq:`fir_filter_f-equation`.

.. math::
   :label: fir_filter_f-equation

   y[n] = \sum_{i=0}^{T-1} b_i * x[n-i]

In :eq:`fir_filter_f-equation`:

 * :math:`x[n]` is the input values.

 * :math:`y[n]` is the output values.

 * :math:`b_i` is the taps values, where there are :math:`T` taps index from :math:`0` to :math:`T - 1`.

A block diagram representation of the implementation is given in :numref:`fir_filter_f-diagram`.

.. _fir_filter_f-diagram:

.. figure:: fir_filter_f.svg
   :alt: Block diagram of finite impulse response (FIR) filter implementation
   :align: center

   FIR filter implementation. :math:`b_i` is the taps values. :math:`x[n]` is the input data. :math:`y[n]` is the output data.

Interface
---------
.. literalinclude:: ../specs/fir_filter_f-spec.xml
   :language: xml
   :lines: 1,19-

Ports
~~~~~
.. ocpi_documentation_ports::

   input: Primary input samples port.
   output: Primary output samples port.

Opcode handling
~~~~~~~~~~~~~~~
The FIR filter is applied to values in sample opcode messages.

When a flush opcode message is received then the filter buffer is flushed. Here the flush behaviour is to give an input of zeros of the length set in the ``flush_length`` property. After the flush has been completed and the resulting sample opcode message sent, the flush message is then forwarded on. When ``flush_length`` is set to zero no sample opcode message will be outputted and the flush message will be passed through without effect.

All other opcodes are passed through this component.

Properties
~~~~~~~~~~
.. ocpi_documentation_properties::

   flush_length: When set to zero no zeros are fed into the filter and flush opcode messages are passed through.

Parameters
~~~~~~~~~~
.. ocpi_documentation_properties::
   :parameters:

Implementations
---------------
.. ocpi_documentation_implementations:: ../fir_filter_f.rcc

Example application
-------------------
.. literalinclude:: example_app.xml
   :language: xml
   :lines: 1,19-

Dependencies
------------
The dependencies to other elements in OpenCPI are:

 * ``components/dsp/common/fir/fir_core.hh``

There is also a dependency on:

 * ``cstddef``

Limitations
-----------
Limitations of ``fir_filter_f`` are:

 * Setting less taps than ``number_of_taps`` is not supported.

Testing
-------
.. ocpi_documentation_test_result_summary::
