// RCC implementation of fir_filter_xf worker.
//
// This file is protected by Copyright. Please refer to the COPYRIGHT file
// distributed with this source distribution.
//
// This file is part of OpenCPI <http://www.opencpi.org>
//
// OpenCPI is free software: you can redistribute it and/or modify it under the
// terms of the GNU Lesser General Public License as published by the Free
// Software Foundation, either version 3 of the License, or (at your option) any
// later version.
//
// OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
// WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
// A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
// more details.
//
// You should have received a copy of the GNU Lesser General Public License
// along with this program. If not, see <http://www.gnu.org/licenses/>.

#include "../common/fir/fir_core.hh"
#include "fir_filter_xf-worker.hh"

using namespace OCPI::RCC;  // for easy access to RCC data types and constants
using namespace Fir_filter_xfWorkerTypes;

class Fir_filter_xfWorker : public Fir_filter_xfWorkerBase {
  fir_core<float> *m_fir = NULL;
  bool data_flushed = false;  // Flags when flush data has been handled, and
                              // flush opcode should be passed
  uint8_t flush_length = 0;
  const Complex_float_timed_sampleSampleData zero_input_data[UINT8_MAX] = {
      {0, 0}};

  RCCResult start() {
    m_fir = new fir_core<float>(properties().number_of_taps, properties().taps);
    return RCC_OK;
  }

  RCCResult stop() {
    if (m_fir != NULL) delete m_fir;
    return RCC_OK;
  }

  RCCResult taps_written() {
    if (m_fir != NULL) {
      m_fir->set_taps(properties().number_of_taps, properties().taps);
    }
    return RCC_OK;
  }

  RCCResult flush_length_written() {
    flush_length = properties().flush_length;
    return RCC_OK;
  }

  RCCResult run(bool) {
    // Handle the most common opCode first
    if (input.opCode() == Complex_float_timed_sampleSample_OPERATION) {
      const Complex_float_timed_sampleSampleData *inputData =
          input.sample().data().data();
      Complex_float_timed_sampleSampleData *outputData =
          output.sample().data().data();

      output.setOpCode(Complex_float_timed_sampleSample_OPERATION);
      if (input.length() == 0) {
        output.setLength(input.sample().data().size());
      } else {
        output.setLength(input.sample().data().size() *
                         sizeof(Complex_float_timed_sampleSampleData));
        m_fir->do_work_complex(&inputData->real, input.sample().data().size(),
                               &outputData->real);
      }
      return RCC_ADVANCE;
    } else if (input.opCode() == Complex_float_timed_sampleTime_OPERATION) {
      // Pass through time opcode and time data
      output.setOpCode(Complex_float_timed_sampleTime_OPERATION);
      output.time().fraction() = input.time().fraction();
      output.time().seconds() = input.time().seconds();
      return RCC_ADVANCE;
    } else if (input.opCode() ==
               Complex_float_timed_sampleSample_interval_OPERATION) {
      // Pass through sample interval opcode and sample interval data
      output.setOpCode(Complex_float_timed_sampleSample_interval_OPERATION);
      output.sample_interval().fraction() = input.sample_interval().fraction();
      output.sample_interval().seconds() = input.sample_interval().seconds();
      return RCC_ADVANCE;
    } else if (input.opCode() == Complex_float_timed_sampleFlush_OPERATION) {
      if (data_flushed == false) {
        if (flush_length > 0) {
          // When a flush is requested input the set number of zeros
          const Complex_float_timed_sampleSampleData *inputData =
              zero_input_data;
          output.setOpCode(Complex_float_timed_sampleSample_OPERATION);
          Complex_float_timed_sampleSampleData *outputData =
              output.sample().data().data();
          // As the maximum flush_length is 255 (uint8_t max) this will fit into
          // a single message; however due to OpenCPI buffer allocation limit of
          // 8,192 for complex floats this is further limited to 1,024.
          output.setLength(flush_length *
                           sizeof(Complex_float_timed_sampleSampleData));
          m_fir->do_work_complex(&inputData->real, flush_length,
                                 &outputData->real);
          output.advance();  // Advance output only as no more input data is
                             // needed until flush opcode sent.
        }
        data_flushed = true;
        return RCC_OK;
      } else {
        // Pass through flush opcode
        output.setOpCode(Complex_float_timed_sampleFlush_OPERATION);
        data_flushed = false;
        return RCC_ADVANCE;
      }
    } else if (input.opCode() ==
               Complex_float_timed_sampleDiscontinuity_OPERATION) {
      // Pass through discontinuity opcode
      output.setOpCode(Complex_float_timed_sampleDiscontinuity_OPERATION);
      return RCC_ADVANCE;
    } else if (input.opCode() == Complex_float_timed_sampleMetadata_OPERATION) {
      // Pass through metadata opcode, id, and data
      output.setOpCode(Complex_float_timed_sampleMetadata_OPERATION);
      output.metadata().id() = input.metadata().id();
      output.metadata().value() = input.metadata().value();
      return RCC_ADVANCE;
    } else {
      setError("Unknown OpCode Received");
      return RCC_FATAL;
    }
  }
};

FIR_FILTER_XF_START_INFO
// Insert any static info assignments here (memSize, memSizes, portInfo)
// e.g.: info.memSize = sizeof(MyMemoryStruct);
FIR_FILTER_XF_END_INFO
