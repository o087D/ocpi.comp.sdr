#!/usr/bin/env python3

# Runs checks for frequency_demodulator_xs_s implementation
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.


import os
import sys

import ocpi_testing

from frequency_demodulator import FrequencyDemodulator


input_file_path = str(sys.argv[-1])
output_file_path = str(sys.argv[-2])

# Initiate frequency demodulator object
frequency_demodulator_implementation = FrequencyDemodulator()

test_id = ocpi_testing.get_test_case()

# Check the result
verifier = ocpi_testing.Verifier(frequency_demodulator_implementation)
verifier.set_port_types(["complex_short_timed_sample"], ["short_timed_sample"],
                        ["bounded_with_exception"])
verifier.comparison[0].WRAP_ROUND_VALUES = [-32768, 32767]
verifier.comparison[0].EXCEPTION_BOUND = 850

subcase = os.environ["OCPI_TEST_subcase"]

if subcase == "near_zero":
    # On average, pairs of complex input values with magnitudes > 2 deviate
    # less than 300 from the expected output values for the HDL implementation
    verifier.comparison[0].BOUND = 300
    # For the near_zero case, random input values with a maximum magnitude of
    # 5 are used. Empirical measurements show that on average values with a
    # magnitude of less than 2 will exceed the bound. Assuming random
    # distribution of input magnitudes, about 2/5 of values will exceed
    # the lower bound.
    verifier.comparison[0].ALLOWED_EXCEPTION_RATE = 2 / 5
else:
    # On average, pairs of complex input values with magnitudes > 50 deviate
    # less than 2 from the expected output values for the HDL implementation
    verifier.comparison[0].BOUND = 2
    # Random input values with a maximum magnitude of (2^15) - 1 are used.
    # Empirical measurements show that on average values with a magnitude of
    # less than 50 will exceed the bound. Assuming random distribution of
    # input magnitudes, about 50/((2^15)-1) of values will exceed the lower
    # bound.
    verifier.comparison[0].ALLOWED_EXCEPTION_RATE = 50 / ((2**15)-1)

if verifier.verify(test_id, [input_file_path], [output_file_path]) is True:
    sys.exit(0)
else:
    sys.exit(1)
