-- frequency_modulator_l_xs HDL implementation.
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

library IEEE;
use IEEE.std_logic_1164.all;
use ieee.numeric_std.all;
library ocpi;
use ocpi.types.all;
library sdr_dsp;
use sdr_dsp.sdr_dsp.cordic_dds;

architecture rtl of frequency_modulator_l_xs_worker is
  -- Delay to align data with control flags
  constant delay_c                : integer := 16;
  constant output_size_c          : integer := 16;  -- i & q data widths
  constant dds_phase_accum_size_c : integer := 32;  -- Width of DDS

  signal take        : std_logic;       -- true when taking input data
  signal enable      : std_logic;       -- true when output can take data
  signal valid_data  : std_logic;  -- true when complex input data is valid
  signal dds_step    : std_logic_vector(dds_phase_accum_size_c - 1 downto 0);
  signal dds_sin_out : std_logic_vector(output_size_c - 1 downto 0);
  signal dds_cos_out : std_logic_vector(output_size_c - 1 downto 0);
  signal data_out    : std_logic_vector((output_size_c * 2) - 1 downto 0);

begin

  -- take input when both input and output are ready
  take           <= ctl_in.is_operating and input_in.ready and output_in.ready;
  input_out.take <= take;

  valid_data <= '1' when take = '1' and input_in.valid = '1' and
                input_in.opcode = long_timed_sample_sample_op_e else '0';

  enable <= ctl_in.is_operating and output_in.ready;

  dds_step <= input_in.data when valid_data = '1' else (others => '0');

  -- Interface delay module
  -- Delays streaming interface signals to align with the delay introduced by
  -- the cordic_dds module.
  interface_delay_i : entity work.long_to_complex_short_protocol_delay
    generic map (
      DELAY_G              => delay_c,
      DATA_IN_WIDTH_G      => input_in.data'length,
      PROCESSED_DATA_MUX_G => '1'
      )
    port map (
      clk                 => ctl_in.clk,
      reset               => ctl_in.reset,
      enable              => enable,
      take_in             => take,
      input_in            => input_in,
      processed_stream_in => data_out,
      output_out          => output_out
      );

  -- DDS module
  dds_inst : cordic_dds
    generic map (
      output_size_g      => output_size_c,
      phase_accum_size_g => dds_phase_accum_size_c
      )
    port map (
      clk        => ctl_in.clk,
      rst        => ctl_in.reset,
      clk_en     => enable,
      step_size  => dds_step,
      gain_in    => std_logic_vector(props_in.modulator_amplitude),
      sine_out   => dds_sin_out,
      cosine_out => dds_cos_out
      );

  data_out <= dds_sin_out & dds_cos_out;

end rtl;
