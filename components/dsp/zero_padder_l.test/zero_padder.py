#!/usr/bin/env python3

# Python implementation of zero padder block for verification
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.


import ocpi_testing


class ZeroPadder(ocpi_testing.Implementation):
    def __init__(self, samples):
        super().__init__(samples=samples)
        self._max_message_samples = 4096

    def reset(self):
        pass

    def sample(self, data):
        output_length = len(data) * (self.samples + 1)
        output = [0] * output_length

        for index, value in enumerate(data):
            index = index * (self.samples + 1)
            output[index] = value
            output[index + 1: index + self.samples + 1] = [0] * (self.samples)

        # Sample messages cannot have more than 4096 samples in each message.
        # As this component increases the data size, break long messages up
        # into messages with no more than 4096 samples.
        messages = [{
            "opcode": "sample",
            "data": output[index: index + self._max_message_samples]}
            for index in range(0, len(output),
                               self._max_message_samples)]

        return self.output_formatter(messages)
