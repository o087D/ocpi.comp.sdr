.. prbs_generator_b documentation

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


.. _prbs_generator_b:


Pseudo-random binary sequence (PRBS) generator (``prbs_generator_b``)
=====================================================================
Generates a stream of pseudo-random boolean symbols.

Design
------
A pseudo-random binary sequence (PRBS) is a deterministic sequence that is statistically similar to a truly random sequence.

PRBS generators are typically implemented using Linear Feedback Shift Registers (LFSR). A polynomial specifies which taps of the LFSR are used as feedback to generate the next input. Polynomials that generate maximum length sequences are typically used as they provide the longest period before the PRBS pattern repeats. The length of a maximum length PRBS pattern is :math:`2^n-1`, where :math:`n` is the order of the LFSR polynomial.

Two examples of the LSFR PRBS generator implementation, for different polynomials are in :numref:`prbs_lfsr_xn_xd_1-diagram` and :numref:`prbs_lfsr_xn_xe_xd_1-diagram`.

.. _prbs_lfsr_xn_xd_1-diagram:

.. figure:: prbs_lfsr_xn_xd_1.svg
   :alt: Arrangement of feedback registers to implement PRBS generator.
   :align: center

   Fibonacci style LSFR PRBS generator implementing the polynomial :math:`x^n + x^d + 1`.

.. _prbs_lfsr_xn_xe_xd_1-diagram:

.. figure:: prbs_lfsr_xn_xe_xd_1.svg
   :alt: Arrangement of feedback registers to implement PRBS generator.
   :align: center

   Fibonacci style LSFR PRBS generator implementing the polynomial :math:`x^n + x^e + x^d + 1`.

In both :numref:`prbs_lfsr_xn_xd_1-diagram` and :numref:`prbs_lfsr_xn_xe_xd_1-diagram`, at the end of the chain of shift registers :math:`y` is the output point.

In some applications it is desirable to start the PRBS at a specific point in the sequence. This can be done via setting the ``initial_seed`` property.
Additionally, in some applications is is desirable to invert the feedback used by the LFSR. This can be done via the ``invert_output`` property.

A PRBS will output all possible :math:`n` bit values except the all zeros pattern, or if the ``invert_output`` property is ``true``, the all ones pattern.

When ``invert_output`` is ``false``, if the ``initial_seed`` property is set to zero, the PRBS generator will only output zeros. When ``invert_output`` is ``true``, if the ``initial_seed`` property is set to all ones, the PRBS generator will only output ones. For all other values of ``initial_seed`` the PRBS generator will output the sequence specified by the ``polynomial_taps`` property starting at the specified seed value.

In this application the most significant bit of the LFSR is used as the source of the PRBS, and is output as a boolean stream.

Interface
---------
.. literalinclude:: ../specs/prbs_generator_b-spec.xml
   :language: xml
   :lines: 1,19-

Ports
~~~~~
.. ocpi_documentation_ports::

   output: Primary output samples port.

Opcode handling
~~~~~~~~~~~~~~~
The worker will output a zero length message with the discontinuity opcode whenever the ``enable`` property is changed from ``true`` to ``false``. This is because the PRBS generator is reset when disabled which causes a discontinuity in the PRBS.

Properties
~~~~~~~~~~
.. ocpi_documentation_properties::

Parameters
~~~~~~~~~~
.. ocpi_documentation_properties::
   :parameters:

   polynomial_taps: Is an array of up to six unsigned chars, meaning a maximum of six taps can be specified. If the PRBS polynomial is :math:`x^7+x^6+1` then ``polynomial_taps`` should be set to ``7,6``. There is no need to specify the :math:`x^0` term as this is implied.

The below table gives the polynomials the component is built for.

+-----------------------------------------------+---------------------+---------------+
| Polynomial                                    | ``polynomial_taps`` | Period        |
+===============================================+=====================+===============+
| :math:`x^7 + x^6 + 1`                         |                7, 6 |           127 |
+-----------------------------------------------+---------------------+---------------+
| :math:`x^9 + x^5 + 1`                         |                9, 5 |           511 |
+-----------------------------------------------+---------------------+---------------+
| :math:`x^{11} + x^9 + 1`                      |               11, 9 |         2,047 |
+-----------------------------------------------+---------------------+---------------+
| :math:`x^{15} + x^{14} + 1`                   |              15, 14 |        32,767 |
+-----------------------------------------------+---------------------+---------------+
| :math:`x^{16} + x^{14} + x^{13} + x^{11} + 1` |      16, 14, 13, 11 |        65,535 |
+-----------------------------------------------+---------------------+---------------+
| :math:`x^{17} + x^{14} + 1`                   |              17, 14 |       131,071 |
+-----------------------------------------------+---------------------+---------------+
| :math:`x^{20} + x^3 + 1`                      |               20, 3 |     1,948,575 |
+-----------------------------------------------+---------------------+---------------+
| :math:`x^{23} + x^{18} + 1`                   |              23, 18 |     8,388,607 |
+-----------------------------------------------+---------------------+---------------+
| :math:`x^{29} + x^{27} + 1`                   |              29, 27 |   536,870,911 |
+-----------------------------------------------+---------------------+---------------+
| :math:`x^{31} + x^{28} + 1`                   |              31, 28 | 2,147,483,647 |
+-----------------------------------------------+---------------------+---------------+

Implementations
---------------
.. ocpi_documentation_implementations:: ../prbs_generator_b.hdl

Example application
-------------------
.. literalinclude:: example_app.xml
   :language: xml
   :lines: 1,19-

Dependencies
------------
The dependencies to other elements in OpenCPI are:

 * :ref:`PRBS generator primitive <prbs_generator-primitive>`

 * :ref:`Protocol interface generator primitive <protocol_interface_generator-primitive>`

There is also a dependency on:

 * ``ieee.std_logic_1164``

 * ``ieee.numeric_std``

Limitations
-----------
Limitations of ``prbs_generator_b`` are:

 * A maximum of 6 taps can be specified. i.e. :math:`x^a + x^b + x^c + x^d + x^e + x^f + 1`

Testing
-------
.. ocpi_documentation_test_result_summary::
