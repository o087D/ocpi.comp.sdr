-- HDL Implementation of a carrier tone generator.
--
-- This file is protected by Copyright. Please refer to the COPYRIGHT file
-- distributed with this source distribution.
--
-- This file is part of OpenCPI <http://www.opencpi.org>
--
-- OpenCPI is free software: you can redistribute it and/or modify it under the
-- terms of the GNU Lesser General Public License as published by the Free
-- Software Foundation, either version 3 of the License, or (at your option) any
-- later version.
--
-- OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
-- WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
-- A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
-- more details.
--
-- You should have received a copy of the GNU Lesser General Public License
-- along with this program. If not, see <http://www.gnu.org/licenses/>.

-- Generates a carrier tone at a user settable frequency

library IEEE;
use IEEE.std_logic_1164.all;
use ieee.numeric_std.all;
library ocpi;
use ocpi.types.all;
library sdr_dsp;
use sdr_dsp.sdr_dsp.cordic_dds;
library sdr_interface;
use sdr_interface.sdr_interface.protocol_interface_generator;

architecture rtl of carrier_generator_xs_worker is

  constant OPCODE_WIDTH_C : integer := 3;

  function slv_to_opcode(inslv : in std_logic_vector(OPCODE_WIDTH_C - 1 downto 0)) return complex_short_timed_sample_OpCode_t is
  begin
    case inslv is
      when "100" =>
        return complex_short_timed_sample_discontinuity_op_e;
      when others =>
        return complex_short_timed_sample_sample_op_e;
    end case;
  end function;

  ------------------------------------------------------------------------------
  -- DDS Constants
  ------------------------------------------------------------------------------
  -- Sets the output bit width of the sine and cosine signals
  constant dds_output_size_c            : integer := 16;
  -- Sets the phase accumulator size
  constant dds_phase_accumulator_size_c : integer := 32;

  ------------------------------------------------------------------------------
  -- Interface Constants
  ------------------------------------------------------------------------------
  -- Number of clock cycles it takes for a change in step size or dds gain to
  -- reach the output.
  constant DELAY_C : integer := dds_output_size_c + 1;

  ------------------------------------------------------------------------------
  -- DDS Signals
  ------------------------------------------------------------------------------
  signal dds_gain    : std_logic_vector(dds_output_size_c -1 downto 0);
  signal dds_step    : std_logic_vector(dds_phase_accumulator_size_c - 1 downto 0);
  signal dds_sin_out : std_logic_vector(dds_output_size_c -1 downto 0);
  signal dds_cos_out : std_logic_vector(dds_output_size_c -1 downto 0);
  signal data_out    : std_logic_vector(output_out.data'length - 1 downto 0);

  ------------------------------------------------------------------------------
  -- Interface Constants
  ------------------------------------------------------------------------------
  signal enable        : std_logic;
  signal output_ready  : std_logic;
  signal output_hold   : std_logic;
  signal discontinuity : std_logic;
  signal output_opcode : std_logic_vector(OPCODE_WIDTH_C - 1 downto 0);
  signal output_data   : std_logic_vector(output_out.data'length - 1 downto 0);

begin

  output_ready <= output_in.ready and ctl_in.is_operating;
  -- Enable the DDS when enable property is true and output is ready, but not
  -- when the interface module is outputting other message signals.
  enable       <= output_ready and props_in.enable and not output_hold;

  dds_step <= std_logic_vector(props_in.step_size);
  dds_gain <= std_logic_vector(props_in.carrier_amplitude(dds_output_size_c - 1 downto 0));

  -- Triggers the insertion of a discontinuity message into the output
  -- data interface.
  discontinuity <= '1' when (props_in.step_size_written = '1' and
                             props_in.discontinuity_on_step_size_change = '1') or
                   (props_in.carrier_amplitude_written = '1' and
                    props_in.discontinuity_on_carrier_amplitude_change = '1') else '0';

  ------------------------------------------------------------------------------
  -- Instantiate DDS module
  ------------------------------------------------------------------------------
  dds_inst : cordic_dds
    generic map (
      output_size_g      => dds_output_size_c,
      phase_accum_size_g => dds_phase_accumulator_size_c
      )
    port map (
      clk        => ctl_in.clk,
      rst        => ctl_in.reset,
      clk_en     => enable,
      step_size  => dds_step,
      gain_in    => dds_gain,
      sine_out   => dds_sin_out,
      cosine_out => dds_cos_out
      );

  data_out <= std_logic_vector(RESIZE(signed(dds_sin_out), output_out.data'length/2)) &
              std_logic_vector(RESIZE(signed(dds_cos_out), output_out.data'length/2));

  interface_generator_i : protocol_interface_generator
    generic map (
      DELAY_G                 => DELAY_C,
      DATA_WIDTH_G            => output_out.data'length,
      OPCODE_WIDTH_G          => OPCODE_WIDTH_C,
      BYTE_ENABLE_WIDTH_G     => output_out.byte_enable'length,
      PROCESSED_DATA_OPCODE_G => "000",
      DISCONTINUITY_OPCODE_G  => "100"
      )
    port map (
      clk                   => ctl_in.clk,
      reset                 => ctl_in.reset,
      output_ready          => output_ready,
      discontinuity_trigger => discontinuity,
      generator_enable      => props_in.enable,
      generator_reset       => '0',
      processed_stream_in   => data_out,
      message_length        => props_in.message_length,
      output_hold           => output_hold,
      output_som            => output_out.som,
      output_eom            => output_out.eom,
      output_valid          => output_out.valid,
      output_give           => output_out.give,
      output_byte_enable    => output_out.byte_enable,
      output_opcode         => output_opcode,
      output_data           => output_data
      );
  output_out.data   <= output_data;
  output_out.opcode <= slv_to_opcode(output_opcode);

end rtl;
