import sys
import os
import pathlib
import importlib

from subprocess import Popen, PIPE

# Get the opencpi install to access built in tooling
process = Popen(['git', 'clone', 'https://gitlab.com/o4ba2/opencpi', 'opencpi'], stdout=PIPE, stderr=PIPE)
_, _ = process.communicate()
sys.path.append(os.path.join(str(pathlib.Path(__file__).resolve().absolute().parent),"opencpi/tools/python"))

# Add the documentation path to access the ocpi_sphinx_ext extension
ocpidoc_path = importlib.util.find_spec('ocpi_documentation')
sys.path.append(str(pathlib.Path(ocpidoc_path.origin).resolve().absolute().parent))


###### Copied from opencpi/tools/python/_opencpi/ocpidoc/ocpi_documentation/conf.py ######

project = "OpenCPI assets"

show_authors = False

version = ""
release = ""

needs_sphinx = "2.4"
extensions = [
    "sphinx.ext.autodoc",
    "sphinx.ext.napoleon",
    "sphinx.ext.mathjax",
    "sphinxcontrib.spelling",
    "ocpi_sphinx_ext"
]

templates_path = []
source_suffix = ".rst"
language = "en"
exclude_patterns = [f"_build/html*", # Default readthedocs output dir
                    "**ocpi_documentation/ocpi_documentation*",
                    "**/exports/*",
                    "**/imports/*",
                    "**/lib/*",
                    "exports/**",
                    "imports/**",
                    "lib/**",
                    "opencpi/**"]

pygments_style = "sphinx"

math_number_all = False

numfig = True
