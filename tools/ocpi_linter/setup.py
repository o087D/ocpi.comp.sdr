#!/usr/bin/env python3

# Install OpenCPI Component Linter
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.


import setuptools


setuptools.setup(
    name="ocpi_linter",
    version="1.0.1",
    description="OpenCPI asset code formatter and checker.",
    python_requires=">=3.6",
    packages=setuptools.find_packages(exclude=["test", "doc"]),
    package_data={"": ["license_notices/*.txt", "vhdl_format.el"]},
    scripts=["ocpilint"],
    classifiers=["Intended Audience :: Developers",
                 "Natural Language :: English"
                 "Operating System :: OS Independent",
                 "Programming Language :: Python :: 3.6"],
    keywords="OpenCPI OCPI linter formatting"
)
