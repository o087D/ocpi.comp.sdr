#!/usr/bin/env python3

# Linter for OpenCPI Projects
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.


import pathlib
import glob


from . import any_code_checker
from . import cpp_code_checker
from . import python_code_checker
from . import rst_code_checker
from . import vhdl_code_checker
from . import xml_code_checker
from . import utilities


# File extensions that are okay, but not linted. No extension option to allow
# for Makefiles and similar.
ALLOWED_BUT_UNCHECKED_FILE_EXTENSIONS = [
    ".svg", ".log", ".pdf", ".html", ".mk", ""]


class OcpiLinter:
    def __init__(self, paths, recursive, project=False):
        """ Initialise a OcpiLinter instance

        Determines the files to be check during initialisation of class.

        Args:
            paths (list): The files or directories to be checked.
            recursive (bool): True if a recursive file search is to be
                completed, False if not.
            project (bool, optional): When True means path is a project path
                and the whole project is to be linted. When set to True
                recursive is ignored. When False, use path and recursive to
                determine which files to lint.

        Returns:
            Initialised OcpiLinter instance.
        """
        self._lint_project = project

        self.files_to_check = []
        if project is False:
            for path in paths:
                path = pathlib.Path(path).expanduser().resolve()
                if path.is_file():
                    self.files_to_check.append(str(path))
                elif path.is_dir():
                    if recursive:
                        for search_path in path.glob("**/*.*"):
                            if search_path.is_file():
                                self.files_to_check.append(str(search_path))
                    else:
                        for search_path in path.glob("*.*"):
                            if search_path.is_file():
                                self.files_to_check.append(str(search_path))
                else:
                    print(f"{path} not a valid file or directory, will not be "
                          + "checked / formatted.")
        else:
            self._project_file_search(paths[0])
            self._project_path = pathlib.Path(paths[0])
            self._project_log = utilities.ProjectLintLogHandler(
                pathlib.Path(paths[0]).joinpath("linter.log"))

    def lint(self, quick=False):
        """ Run formatters and code checkers

        Args:
            quick (bool, optional): When set to True disables tests which
                require user checks or confirmation.

        Returns:
            Integer to mark the number of identified linter issues.
        """
        total_issue_count = 0

        for file in self.files_to_check:
            print(utilities.PrintStyle.BOLD + f"Checking file: {file}" +
                  utilities.PrintStyle.NORMAL)

            linting_result = self._lint_file(file, quick)

            # Always update the files linting log, if it has one.
            log = utilities.LintLogHandler(file)
            for test_number, test in linting_result.items():
                log.record(test_number, test["name"], test["issue_count"])
                total_issue_count = total_issue_count + test["issue_count"]

            # For projects update the project linting log
            if self._lint_project is True:
                for test_number, test in linting_result.items():
                    self._project_log.write(file, test_number, test["name"],
                                            test["issue_count"])

        return total_issue_count

    def _lint_file(self, path, quick):
        """ Lint a file

        Args:
            path (str): Path of the file to be linted.
            quick (bool): Indicate if a quick test, which is a test without
                user questions, is to be run when set to True. Or a full test
                with user questions is to be run when set to False.

        Returns:
            A dictionary with the test results.
        """
        file_extension = pathlib.Path(path).suffix

        # No checks for some files types
        if file_extension in ALLOWED_BUT_UNCHECKED_FILE_EXTENSIONS:
            return {}

        if file_extension in cpp_code_checker.CPP_FILE_EXTENSIONS:
            language_checker = cpp_code_checker.CppCodeChecker(path)
        elif file_extension in python_code_checker.PYTHON_FILE_EXTENSIONS:
            language_checker = python_code_checker.PythonCodeChecker(path)
        elif file_extension in rst_code_checker.RST_FILE_EXTENSIONS:
            language_checker = rst_code_checker.RstCodeChecker(path)
        elif file_extension in vhdl_code_checker.VHDL_FILE_EXTENSIONS:
            language_checker = vhdl_code_checker.VhdlCodeChecker(path)
        elif file_extension in xml_code_checker.XML_FILE_EXTENSIONS:
            language_checker = xml_code_checker.XmlCodeChecker(path)
        else:
            print(f"{file_extension} not a recognised file extension (case "
                  + "sensitive) and so should not be used. File will not be "
                  "formatted or checked.\n"
                  + f"  Caused by file: {path}")
            return {"any_999": {"name": "File extension", "issue_count": 1}}

        issues = language_checker.lint(quick)

        # Run language agnostic checks, run these after language specific
        # checks since the language formatters will do things such as remove
        # trailing blank spaces which is checked with the language agnostic
        # checker.
        issues.update(any_code_checker.AnyCodeChecker(path).lint(quick))

        return issues

    def _project_file_search(self, project_path):
        """ Find all files in an OpenCPI project to be linted

        The files found are stored in the list ``self.files_to_check``.

        Args:
            project_path (str): The project directory of the project path.
        """
        search_patterns = ["components/*/*.comp/*.rst",
                           "components/*/*.comp/*.xml",
                           "components/*/*.hdl/*.vhd",
                           "components/*/*.hdl/*.xml",
                           "components/*/*.rcc/*.cc",
                           "components/*/*.rcc/*.hh",
                           "components/*/*.rcc/*.xml",
                           "components/*/*.test/*.xml",
                           "components/*/*.test/*.py",
                           "components/*/common/*/*.hh",
                           "components/*/common/*/*.cc",
                           "components/*/common/*.hh",
                           "components/*/common/*.cc",
                           "components/*/specs/*.xml",
                           "components/*.comp/*.rst",
                           "components/*.hdl/*.vhd",
                           "components/*.hdl/*.xml",
                           "components/*.rcc/*.cc",
                           "components/*.rcc/*.hh",
                           "components/*.rcc/*.xml",
                           "components/*.test/*.xml",
                           "components/*.test/*.py",
                           "components/common/*/*.hh",
                           "components/common/*/*.cc",
                           "components/common/*.hh",
                           "components/common/*.cc",
                           "components/specs/*.xml",
                           "hdl/primitives/*/*/src/*.vhd",
                           "hdl/primitives/*/*/*.rst",
                           "specs/*.rst",
                           "specs/*.xml",
                           "tools/**/*.py",
                           "tools/**/*.rst",
                           "tools/**/*.inc",
                           "tools/**/*.xml",
                           "tools/**/*.cc",
                           "tools/**/*.hh",
                           "tools/**/*.vhd"]
        self.files_to_check = []
        project_directory = pathlib.Path(project_path)
        for pattern in search_patterns:
            for path in project_directory.glob(pattern):
                if path.is_file():
                    self.files_to_check.append(str(path.resolve().absolute()))
