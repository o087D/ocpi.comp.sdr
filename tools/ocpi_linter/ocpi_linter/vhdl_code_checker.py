#!/usr/bin/env python3

# Runs code checks on VHDL source code files
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.


import pathlib
import subprocess

from . import base_code_checker
from . import utilities


VHDL_FILE_EXTENSIONS = [".vhd"]


class VhdlCodeChecker(base_code_checker.BaseCodeChecker):
    """ Code formatter and checker for VHDL
    """

    def test_vhdl_000(self):
        """ Run Emacs formatting on code

        **Test name:** Emacs formatting

        Returns:
            A tuple containing a string with the test name and a list of the
            identified test issues.
        """
        test_name = "Emacs formatting"

        if self._check_installed("emacs"):
            emacs_lisp_file = str(pathlib.Path(__file__).parent.joinpath(
                "vhdl_format.el"))
            # As Emacs is used for formatting, what it reports the to terminal
            # can be ignored. Therefore pipe output to NULL, as Emacs prints
            # messages during formatting stderr so this is also needs to be
            # # redirected to hide these messages.
            process = subprocess.Popen([
                "emacs", "-batch", "-l", emacs_lisp_file, "--visit", self.path,
                "-f", "vhdl-format"], stdout=subprocess.DEVNULL,
                stderr=subprocess.DEVNULL)
            process.wait()

            # As file may have changed re-read in code
            self._read_in_code()

            return test_name, []

        else:
            return test_name, [
                {"line": None,
                 "message": "Emacs not installed. Cannot run test."}]

    def test_vhdl_001(self):
        """ Check the first lines in file are in the correct format

        **Test name:** Correct opening lines

        Returns:
            A tuple containing a string with the test name and a list of the
            identified test issues.
        """
        test_name = "Correct opening lines"

        issues = []

        if len(self._code) < 14:
            issues = [{"line": None, "message": "Full header is not in file."}]
            return test_name, issues

        if self._code[0][0:2] != "--":
            issues.append({"line": 1,
                           "message": "Must be brief description."})

        if self._code[1] != "--":
            issues.append({"line": 2,
                           "message": "Must be blank comment line."})

        # License notice
        line_number = 2
        with open(pathlib.Path(__file__).parent.joinpath(
                "license_notices").joinpath("vhdl.txt"),
                "r") as license_notice:
            if (len(self._code) - line_number) < sum(
                    1 for line in license_notice):
                issues.append({
                    "line": None,
                    "message": "File is not large enough to include license "
                               + "notice."})
                return test_name, issues
            license_notice.seek(0)
            for license_line in license_notice:
                if self._code[line_number] != license_line.strip("\n"):
                    issues.append({
                        "line": line_number + 1,
                        "message": "License notice is incorrect, should be \""
                                   + license_line.strip("\n") + "\"."})
                line_number = line_number + 1

        # Blank line
        if self._code[line_number] != "":
            issues.append({
                "line": line_number + 1,
                "message": "Blank line does not follow license notice."})

        return test_name, issues

    def test_vhdl_002(self):
        """ Check there is a single space after the comment symbol

        **Test name:** Comment space

        Returns:
            A tuple containing a string with the test name and a list of the
            identified test issues.
        """
        test_name = "Comment space"

        issues = []
        for line_number, line_text in enumerate(self._code):
            comment_start = line_text.find("--")
            # If does not contain a comment ignore line
            if comment_start == -1:
                continue
            # If nothing after the comment "--", the line can be ignored from
            # further checks as comment without value after is allowed
            if len(line_text) - comment_start <= 2:
                continue
            if line_text[comment_start + 2] != " ":
                # Ignore lines which are all comment symbols
                if all([char is "-" for char in line_text[comment_start:-1]]):
                    continue
                issues.append({
                    "line": line_number + 1,
                    "message": "No space after comment flag (\"--\")."})

        return test_name, issues

    def test_vhdl_003(self):
        """ Check comment lines do not exceed 80 characters

        **Test name:** Comments within 80 character line

        Returns:
            A tuple containing a string with the test name and a list of the
            identified test issues.
        """
        test_name = "Comments within 80 character line"

        issues = []
        for line_number, line_text in enumerate(self._code):
            if line_text.lstrip()[0:2] == "--":
                if len(line_text) > 80:
                    issues.append({
                        "line": line_number + 1,
                        "message": "Comment lines must not exceed 80 "
                                   + "characters."})

        return test_name, issues

    def user_test_vhdl_500(self):
        """ Ask user if file name is in correct format

        **Test name:** File name

        Returns:
            A tuple containing a string with the test name and a list of the
            identified test issues.
        """
        test_name = "File name"

        return test_name, utilities.ask_user(
            "Is the component name in the correct format?\n" +
            "  (e.g. [description]_[input_protocol]_[output_protocol], " +
            "where description must be a noun and not a verb)",
            bool)

    def user_test_vhdl_501(self):
        """ Ask user if version numbers match

        **Test name:** Version numbers match

        Ask user if version numbers in source code file and worker
        specification XML file.

        Returns:
            A tuple containing a string with the test name and a list of the
            identified test issues.
        """
        test_name = "Version numbers match"

        return test_name, utilities.ask_user(
            "Does the version number in the source match the version number " +
            "in the worker specification?",
            bool)

    def user_test_vhdl_503(self):
        """ Ask user to confirm backpressure is supported

        **Test name:** Backpressure supported

        Backpressure is when the next element in the processing chain cannot
        currently take any inputs and so the signal that data must stop flowing
        or data needs to be buffered passes up the processing chain.

        Returns:
            A tuple containing a string with the test name and a list of the
            identified test issues.
        """
        test_name = "Backpressure supported"

        return test_name, utilities.ask_user(
            "Do streaming interfaces support backpressure?", bool)

    def user_test_vhdl_504(self):
        """ Ask user to confirm start-of-message and end-of-message supported

        **Test name:** Start-of-message / End-of-message support

        Returns:
            A tuple containing a string with the test name and a list of the
            identified test issues.
        """
        test_name = "Start-of-message / End-of-message support"

        return test_name, utilities.ask_user(
            "Do streaming interfaces support start-of-message and end-of-" +
            "message triggers, including early start-of-message and late " +
            "end-of-message?",
            bool)

    def user_test_vhdl_505(self):
        """ Ask user to confirm zero length messages supported

        **Test name:** Zero length message support

        Returns:
            A tuple containing a string with the test name and a list of the
            identified test issues.
        """
        test_name = "Zero length message support"

        return test_name, utilities.ask_user(
            "Do streaming interfaces support zero-length-messages?",
            bool)

    def user_test_vhdl_506(self):
        """ User to confirm handles different ``ctl_in.is_operating`` states

        **Test name:** ``ctl_in.is_operating`` supported

        Returns:
            A tuple containing a string with the test name and a list of the
            identified test issues.
        """
        test_name = "ctl_in.is_operating supported"

        return test_name, utilities.ask_user(
            "Does the implementation stop when ctl_in.is_operating is false?",
            bool)

    def user_test_vhdl_507(self):
        """ Ask user to confirm errors trigger ``ctl_out.error``

        **Test name:** Error is ``ctl_out.error``

        Returns:
            A tuple containing a string with the test name and a list of the
            identified test issues.
        """
        test_name = "Error is ctl_out.error"

        return test_name, utilities.ask_user(
            "Do errors trigger ctl_out.error?", bool)

    def user_test_vhdl_508(self):
        """ Ask user to confirm reset is supported

        **Test name:** Reset supported

        Returns:
            A tuple containing a string with the test name and a list of the
            identified test issues.
        """
        test_name = "Reset supported"

        return test_name, utilities.ask_user(
            "After ctl_in.reset is asserted does the implementation go to " +
            "the same state as at start up?",
            bool)

    def user_test_vhdl_509(self):
        """ Ask user if properties can be changed during run-time

        **Test name:** Run-time property change

        Returns:
            A tuple containing a string with the test name and a list of the
            identified test issues.
        """
        test_name = "Run-time property change"

        return test_name, utilities.ask_user(
            "Can all properties (which are not parameters) be updated " +
            "dynamically?",
            bool)

    def user_test_vhdl_510(self):
        """ Ask user to confirm no magic numbers (unnamed constants) are used

        **Test name:** No magic numbers

        Returns:
            A tuple containing a string with the test name and a list of the
            identified test issues.
        """
        test_name = "No magic numbers"

        return test_name, utilities.ask_user(
            "Are no \"magic numbers\" (unnamed constants) used?", bool)
