#!/usr/bin/env python3

# Question the User via terminal messages and response
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.


def ask_user(question, return_type):
    """ Ask the user a question and get response via the command line

    Ensures the user's response is valid for the return type.

    Args:
        question (str): The question the user is to be asked.
        return_type (type): The return type the user must respond with.

    Returns:
        The user's response as a ``return_type`` type.
    """
    print(question)
    user_return = input()

    if return_type == bool:
        while user_return.upper() not in ["Y", "YES", "N", "NO"]:
            print("The only acceptable entries are yes (y) or no (n).\n"
                  + "Please retry")
            user_return = input()
        if user_return.upper() in ["Y", "YES"]:
            return True
        else:
            return False

    valid_input = False
    while not valid_input:
        try:
            return_type(user_return)
            valid_input = True
        except ValueError:
            valid_input = False
            print(f"The only acceptable entries are {return_type.__name__}.\n"
                  + "Please retry")
            user_return = input()
    return return_type(user_return)
