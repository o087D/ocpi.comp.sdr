#!/usr/bin/env python3

# Runs code checks on Python source code files
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.


import pathlib
import re
import subprocess

from . import base_code_checker
from . import utilities


PYTHON_FILE_EXTENSIONS = [".py"]


class PythonCodeChecker(base_code_checker.BaseCodeChecker):
    """ Code formatter and checker for Python
    """

    def test_py_000(self):
        """ Run AutoPEP8 on code

        **Test name:** AutoPEP8

        Returns:
            A tuple containing a string with the test name and a list of the
            identified test issues.
        """
        test_name = "AutoPEP8"

        if self._check_installed("autopep8"):
            # Option -i means in-place so updates the file
            process = subprocess.Popen(["autopep8", "-i", self.path])
            process.wait()

            # As file may have changed re-read in code
            self._read_in_code()

            return test_name, []   # Cannot return any issues

        else:
            return test_name, [
                {"line": None,
                 "message": "AutoPEP not installed. Cannot run test."}]

    def test_py_001(self):
        """ Run PyCodeStyle on code

        **Test name:** PyCodeStyle

        Returns:
            A tuple containing a string with the test name and a list of the
            identified test issues.
        """
        test_name = "PyCodeStyle"

        if self._check_installed("pycodestyle"):
            process = subprocess.Popen(["pycodestyle", self.path],
                                       stdout=subprocess.PIPE,
                                       stderr=subprocess.PIPE)
            process.wait()
            standard_out = process.communicate()[0]
            pycodestyle_issues = standard_out.decode("utf-8").split("\n")[:-1]

            issues = []
            for issue in pycodestyle_issues:
                if issue.count(":") > 3:
                    line_number = issue.split(":")[1]
                    message = join(":").issue.split(":")[3:]
                    issues.append({"line": line_number, "message": message})

        else:
            issues = [
                {"line": None,
                 "message": "PyCodeStyle not installed. Cannot run test."}]

        return test_name, issues

    def test_py_002(self):
        """ Check the first lines in file are in the correct format

        **Test name:** Correct opening lines

        Returns:
            A tuple containing a string with the test name and a list of the
            identified test issues.
        """
        test_name = "Correct opening lines"

        issues = []

        if len(self._code) < 14:
            issues = [{"line": None, "message": "Full header is not in file."}]
            return test_name, issues

        # Top line must be hash-bang
        if not self._code[0] == "#!/usr/bin/env python3":
            issues.append({
                "line": 1,
                "message": "Python 3 hashbang (#!/usr/bin/env python3) "
                           + "not on first line of file."})

        # Blank line must follow hash-bang
        if self._code[1] != "":
            issues.append({
                "line": 2,
                "messgae": "Blank line must follow hash-bang."})

        # Introduction sentence
        line_number = 2
        while (self._code[line_number][:1] == "#") and (
                len(self._code[line_number]) > 2):
            line_number = line_number + 1

        # Blank comment must follow introduction statement
        if self._code[line_number] != "#":
            issues.append({
                "line": line_number + 1,
                "message": "No blank comment line found after introduction "
                           + "sentence."})
        line_number = line_number + 1

        # License notice
        with open(pathlib.Path(__file__).parent.joinpath(
                "license_notices").joinpath("python.txt"),
                "r") as license_notice:
            if (len(self._code) - line_number) < sum(
                    1 for line in license_notice):
                issues.append({
                    "line": None,
                    "message": "File is not large enough to include license "
                               + "notice."})
                return test_name, issues
            license_notice.seek(0)
            for license_line in license_notice:
                if self._code[line_number] != license_line.strip("\n"):
                    issues.append({
                        "line": line_number + 1,
                        "message": "License notice is incorrect, should be \""
                                   + license_line.strip("\n") + "\"."})
                line_number = line_number + 1

        # Blank line
        if self._code[line_number] != "":
            issues.append({
                "line": line_number + 1,
                "message": "Blank line does not follow license notice."})

        return test_name, issues

    def test_py_003(self):
        """ Check only double quotation marks are used

        **Test name:** Double quotation marks

        Returns:
            A tuple containing a string with the test name and a list of the
            identified test issues.
        """
        test_name = "Double quotation marks"

        reduced_code = self._remove_comments_and_strings()
        issues = []
        for line_number, line_text in enumerate(reduced_code):
            if "'" in line_text:
                issues.append({
                    "line": line_number + 1,
                    "message": "Uses single quotation mark, double should be "
                               + "used."})

        return test_name, issues

    def test_py_004(self):
        """ Check the complex literal is not used

        **Test name:** No complex literal

        The ``complex()`` function rather than the complex literal ``j`` must
        be used, since there are some small differences in how negative zero
        is handled between each case.

        Returns:
            A tuple containing a string with the test name and a list of the
            identified test issues.
        """
        test_name = "No complex literal"

        reduced_code = self._remove_comments_and_strings()
        issues = []
        for line_number, line_text in enumerate(reduced_code):
            if bool(re.search("\\d+\\.?j", line_text)):
                issues.append({
                    "line": line_number + 1,
                    "message": "Uses complex literal (i.e. j). Complex "
                               + "function, not complex literal, must be used "
                               + "for declaring complex values."})

        return test_name, issues

    def _remove_comments_and_strings(self):
        """ Remove comments, docstrings and string content from code

        String quote marks are left but content within string are removed.
        Comment symbol and docstring symbols are removed.

        Uses self._code() as the input code to be filtered.

        Returns:
            List of the code without comments, docstrings or strings.
        """
        docstrings_removed = [""] * len(self._code)

        # Remove docstrings
        currently_docstring = False
        for line_number, line_text in enumerate(self._code):
            # If currently in a docstring, check if ended and add any trailing
            # code
            if currently_docstring:
                if line_text.find("\"\"\"") >= 0:
                    _, docstrings_removed[line_number] = line_text.split(
                        "\"\"\"", 1)
                    currently_docstring = False

            # If not currently a docstring, check if one starts this line
            elif line_text.find("\"\"\"") >= 0:
                docstrings_removed[line_number], docstring = line_text.split(
                    "\"\"\"", 1)
                currently_docstring = True

                # Check docstring doesn't end on same line
                if docstring.find("\"\"\"") >= 0:
                    _, trailing_code = docstring.split("\"\"\"", 1)
                    docstrings_removed[line_number] = (
                        docstrings_removed[line_number] + trailing_code)
                    currently_docstring = False

            # Otherwise not a docstring to record line
            else:
                docstrings_removed[line_number] = line_text

        reduced_code = [""] * len(self._code)
        for line_number, line_text in enumerate(docstrings_removed):
            # Remove comments
            if line_text.find("#") >= 0:
                # Check not part of a string
                if self._find_unescaped_symbol(line_text, "'") is not None:
                    if self._find_unescaped_symbol(
                            line_text, "'") > line_text.find("#"):
                        line_text = line_text[0:line_text.find("#")]
                elif self._find_unescaped_symbol(line_text, "\"") is not None:
                    if self._find_unescaped_symbol(
                            line_text, "\"") > line_text.find("#"):
                        line_text = line_text[0:line_text.find("#")]
                else:
                    line_text = line_text[0:line_text.find("#")]

            # Remove strings
            single_quote = self._find_unescaped_symbol(line_text, "'")
            double_quote = self._find_unescaped_symbol(line_text, "\"")
            while not (single_quote is None and double_quote is None):
                if single_quote is not None and double_quote is not None:
                    if single_quote < double_quote:
                        string_start = single_quote + 1
                        string_symbol = "'"
                    else:
                        string_start = double_quote + 1
                        string_symbol = "\""
                elif single_quote is None:
                    string_start = double_quote + 1
                    string_symbol = "\""
                else:
                    string_start = single_quote + 1
                    string_symbol = "'"
                string_end = self._find_unescaped_symbol(
                    line_text[string_start:], string_symbol)
                if string_end is None:
                    if line_text.find("#", 0, string_start) < 0:
                        raise ValueError(
                            "Cannot find end of string in text, "
                            + f"{line_text[string_start:]}. From line "
                            + f"{line_number + 1}")
                # Add offset for the sub-string searched
                string_end = string_end + string_start
                line_text = (line_text[:string_start] +
                             line_text[string_end:])
                # If there is no more text to search return None, since this is
                # what self._find_unescaped_symbol() would return if the symbol
                # is not found. Otherwise search the rest of the line for any
                # more strings.
                if string_start + 2 >= len(line_text):
                    single_quote = None
                    double_quote = None
                else:
                    # Use string_start as start position for searching as the
                    # string content has been removed now, so string_end is no
                    # longer valid
                    single_quote = self._find_unescaped_symbol(
                        line_text[string_start + 1:], "'")
                    if single_quote is not None:
                        single_quote = single_quote + string_start + 1
                    double_quote = self._find_unescaped_symbol(
                        line_text[string_start + 1:], "\"")
                    if double_quote is not None:
                        double_quote = double_quote + string_start + 1

            reduced_code[line_number] = line_text

        return reduced_code

    def _find_unescaped_symbol(self, text, symbol):
        """ Find a set symbol after a string escape symbol

        If symbol is escaped within search text will not be found.

        Args:
            text (``str``): Text to be searched.
            symbol (``str``): The symbol to be found.

        Returns:
            Index position the unescaped symbol was found in the text. Or
            ``None`` if the symbol is not found.
        """
        # If first character then cannot be escaped, and regex pattern will not
        # find the pattern
        if len(text) > 0:
            if text[0] == symbol:
                return 0

        # Regular expression pattern searches for the symbol but must not have
        # a leading "\"
        search_result = re.search(r"[^\\\\]" + symbol, text)
        if search_result is None:
            return None
        else:
            # Add one because searches for symbol and checks previous character
            return search_result.start() + 1
