#!/usr/bin/env python3

# Test code in xml_code_checker.py
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.


import os
import pathlib
import unittest
import unittest.mock

from ocpi_linter.xml_code_checker import XmlCodeChecker


component_spec_patch_string = (
    "ocpi_linter.xml_code_checker.XmlCodeChecker._component_spec")
worker_description_patch_string = (
    "ocpi_linter.xml_code_checker.XmlCodeChecker._worker_description")
build_file_patch_string = (
    "ocpi_linter.xml_code_checker.XmlCodeChecker._build_file")


class TestXmlCodeChecker(unittest.TestCase):
    def setUp(self):
        self.test_file_path = pathlib.Path("test_file.xml")
        self.test_file_path.touch()

        self.test_checker = XmlCodeChecker(self.test_file_path)

    def tearDown(self):
        if self.test_file_path.is_file():
            os.remove(self.test_file_path)

        # In case test file path is change during test
        if pathlib.Path("test_file.xml").is_file():
            os.remove("test_file.xml")

    # Linter test 000 is not tested here since this uses an external code
    # formatter and it is assumed this is tested separately.

    def test_xml_001_pass(self):
        code_sample = (
            "<?xmlversion=1.0?>\n" +
            "<!-- This file is protected by Copyright. Please refer to the " +
            "COPYRIGHT file\n" +
            "     distributed with this source distribution.\n" +
            "\n" +
            "     This file is part of OpenCPI <http://www.opencpi.org>\n" +
            "\n" +
            "     OpenCPI is free software: you can redistribute it and/or " +
            "modify it under\n" +
            "     the terms of the GNU Lesser General Public License as " +
            "published by the Free\n" +
            "     Software Foundation, either version 3 of the License, or " +
            "(at your option)\n" +
            "     any later version.\n" +
            "\n" +
            "     OpenCPI is distributed in the hope that it will be " +
            "useful, but WITHOUT ANY\n" +
            "     WARRANTY; without even the implied warranty of " +
            "MERCHANTABILITY or FITNESS\n" +
            "     FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public" +
            " License for\n" +
            "     more details.\n" +
            "\n" +
            "     You should have received a copy of the GNU Lesser " +
            "General Public License\n" +
            "     along with this program. If not, see " +
            "<http://www.gnu.org/licenses/>. -->\n" +
            "This would be the start of the main file.")
        with open(self.test_file_path, "w+") as source_code_file:
            source_code_file.write(code_sample)

        code_checker = XmlCodeChecker(self.test_file_path)
        code_checker._read_in_code()

        _, issues = code_checker.test_xml_001()

        self.assertEqual([], issues)

    def test_xml_001_fail_no_header(self):
        code_sample = "A single line of text"
        with open(self.test_file_path, "w+") as source_code_file:
            source_code_file.write(code_sample)

        code_checker = XmlCodeChecker(self.test_file_path)
        code_checker._read_in_code()

        _, issues = code_checker.test_xml_001()

        self.assertEqual(1, len(issues))
        self.assertEqual(1, issues[0]["line"])

    def test_xml_001_fail_license_notice_typo(self):
        code_sample = (
            "<?xmlversion=1.0?>\n" +
            "<!-- This file is protected by Copyright. Please refer to the " +
            "COPYRIGHT file\n" +
            "     distributed with this source distribution.\n" +
            "\n" +
            "     This file is part of OpenCPI <http://www.opencpi.org>\n" +
            "\n" +
            "     OpenCPI is free software: you can redistribute it and/or " +
            "modify it under\n" +
            "     the terms of the GNU Lesser General Public License as " +
            "published by the Frea\n" +
            "     Software Foundation, either version 3 of the License, or " +
            "(at your option)\n" +
            "     any later version.\n" +
            "\n" +
            "     OpenCPI is distributed in the hope that it will be " +
            "useful, but WITHOUT ANY\n" +
            "     WARRANTY; without even the implied warranty of " +
            "MERCHANTABILITY or FITNESS\n" +
            "     FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public" +
            " License for\n" +
            "     more details.\n" +
            "\n" +
            "     You should have received a copy of the GNU Lesser " +
            "General Public License\n" +
            "     along with this program. If not, see " +
            "<http://www.gnu.org/licenses/>. -->\n" +
            "This would be the start of the main file.")
        with open(self.test_file_path, "w+") as source_code_file:
            source_code_file.write(code_sample)

        code_checker = XmlCodeChecker(self.test_file_path)
        code_checker._read_in_code()

        _, issues = code_checker.test_xml_001()

        self.assertEqual(1, len(issues))
        self.assertEqual(8, issues[0]["line"])

    def test_xml_002_pass(self):
        code_sample = ("<!-- 'Single quotes' are fine in comments -->\n" +
                       "<element value=\"something\"/>\n")
        with open(self.test_file_path, "w+") as source_code_file:
            source_code_file.write(code_sample)

        code_checker = XmlCodeChecker(self.test_file_path)
        code_checker._read_in_code()

        _, issues = code_checker.test_xml_002()

        self.assertEqual([], issues)

    def test_xml_002_fail(self):
        code_sample = ("<!-- 'Single quotes' are fine in comments -->\n" +
                       "<element value='something'/>\n")
        with open(self.test_file_path, "w+") as source_code_file:
            source_code_file.write(code_sample)

        code_checker = XmlCodeChecker(self.test_file_path)
        code_checker._read_in_code()

        _, issues = code_checker.test_xml_002()

        self.assertEqual(1, len(issues))
        self.assertEqual(2, issues[0]["line"])

    def test_xml_003_pass(self):
        code_sample = ("<!-- A short comment. -->\n" +
                       "<element value=\"something\"/>\n")
        with open(self.test_file_path, "w+") as source_code_file:
            source_code_file.write(code_sample)

        code_checker = XmlCodeChecker(self.test_file_path)
        code_checker._read_in_code()

        _, issues = code_checker.test_xml_003()

        self.assertEqual([], issues)

    def test_xml_003_fail(self):
        code_sample = (
            "<!-- Some very long comment line. THis line will exceed the 80 " +
            "character line limit. -->\n" +
            "<element value='something'/>\n")
        with open(self.test_file_path, "w+") as source_code_file:
            source_code_file.write(code_sample)

        code_checker = XmlCodeChecker(self.test_file_path)
        code_checker._read_in_code()

        _, issues = code_checker.test_xml_003()

        self.assertEqual(1, len(issues))
        self.assertEqual(1, issues[0]["line"])

    def test_xml_004_pass(self):
        code_sample = (
            "<!-- Upper case allowed in comments. -->\n" +
            "<element_1 value=\"something\"/>\n" +
            "<element_2 description=\"Upper case allowed in description\"/>\n")
        with open(self.test_file_path, "w+") as source_code_file:
            source_code_file.write(code_sample)

        code_checker = XmlCodeChecker(self.test_file_path)
        code_checker._read_in_code()

        _, issues = code_checker.test_xml_004()

        self.assertEqual([], issues)

    def test_xml_004_fail(self):
        code_sample = (
            "<!--A comment -->\n" +
            "<element value=\"Something\"/>\n")
        with open(self.test_file_path, "w+") as source_code_file:
            source_code_file.write(code_sample)

        code_checker = XmlCodeChecker(self.test_file_path)
        code_checker._read_in_code()

        _, issues = code_checker.test_xml_004()

        self.assertEqual(1, len(issues))
        self.assertEqual(2, issues[0]["line"])

    def test_xml_005_pass(self):
        code_sample = (
            "<componentspec>\n" +
            "  <port name=\"input\" producer=\"false\" " +
            "protocol=\"short_v1_protocol\"/>\n" +
            " <port name=\"output\" producer=\"true\" " +
            "protocol=\"short_v1_protocol\"/>\n" +
            " <property name=\"a_property\" type=\"uchar\" " +
            "writable=\"true\" default=\"0\" " +
            "description=\"Describes the property.\"/>\n" +
            "</componentspec>\n")
        with open(self.test_file_path, "w+") as source_code_file:
            source_code_file.write(code_sample)

        code_checker = XmlCodeChecker(self.test_file_path)
        code_checker._read_in_code()

        with unittest.mock.patch(component_spec_patch_string,
                                 return_value=True):
            _, issues = code_checker.test_xml_005()

        self.assertEqual([], issues)

    def test_xml_005_fail_writable_not_set(self):
        code_sample = (
            "<componentspec>\n" +
            "  <port name=\"input\" producer=\"false\" " +
            "protocol=\"short_v1_protocol\"/>\n" +
            " <port name=\"output\" producer=\"true\" " +
            "protocol=\"short_v1_protocol\"/>\n" +
            " <property name=\"a_property\" type=\"uchar\" " +
            "default=\"0\" " +
            "description=\"Describes the property.\"/>\n" +
            "</componentspec>\n")
        with open(self.test_file_path, "w+") as source_code_file:
            source_code_file.write(code_sample)

        code_checker = XmlCodeChecker(self.test_file_path)
        code_checker._read_in_code()

        with unittest.mock.patch(component_spec_patch_string,
                                 return_value=True):
            _, issues = code_checker.test_xml_005()

        self.assertEqual(1, len(issues))
        self.assertEqual(4, issues[0]["line"])

    def test_xml_006_pass(self):
        code_sample = (
            "<componentspec>\n" +
            "  <port name=\"input\" producer=\"false\" " +
            "protocol=\"short_v1_protocol\"/>\n" +
            " <port name=\"output\" producer=\"true\" " +
            "protocol=\"short_v1_protocol\"/>\n" +
            " <property name=\"a_property\" type=\"uchar\" " +
            "readable=\"true\" writable=\"true\" default=\"0\" " +
            "description=\"Describes the property.\"/>\n" +
            "</componentspec>\n")
        with open(self.test_file_path, "w+") as source_code_file:
            source_code_file.write(code_sample)

        code_checker = XmlCodeChecker(self.test_file_path)
        code_checker._read_in_code()

        with unittest.mock.patch(component_spec_patch_string,
                                 return_value=True):
            _, issues = code_checker.test_xml_006()

        self.assertEqual([], issues)

    def test_xml_006_fail(self):
        code_sample = (
            "<componentspec>\n" +
            "  <port name=\"input\" protocol=\"short_v1_protocol\"/>\n" +
            " <port name=\"output\" producer=\"true\" " +
            "protocol=\"short_v1_protocol\"/>\n" +
            " <property name=\"a_property\" type=\"uchar\" " +
            "writable=\"true\" default=\"0\" " +
            "description=\"Describes the property.\"/>\n" +
            "</componentspec>\n")
        with open(self.test_file_path, "w+") as source_code_file:
            source_code_file.write(code_sample)

        code_checker = XmlCodeChecker(self.test_file_path)
        code_checker._read_in_code()

        with unittest.mock.patch(component_spec_patch_string,
                                 return_value=True):
            _, issues = code_checker.test_xml_006()

        self.assertEqual(1, len(issues))
        self.assertEqual(2, issues[0]["line"])

    def test_xml_007_pass(self):
        code_sample = (
            "<hdlworker language=\"vhdl\" spec=\"component-spec\">\n" +
            "  <streaminterface name=\"input\" datawidth=\"8\"/>\n" +
            "  <streaminterface name=\"output\" datawidth=\"8\"/>\n" +
            "  <property name=\"a_worker_property\" parameter=\"true\" " +
            "initial=\"true\" default=\"2\"/>\n" +
            "</hdlworker>\n")
        with open(self.test_file_path, "w+") as source_code_file:
            source_code_file.write(code_sample)

        code_checker = XmlCodeChecker(self.test_file_path)
        code_checker._read_in_code()

        with unittest.mock.patch(worker_description_patch_string,
                                 return_value=True):
            _, issues = code_checker.test_xml_007()

        self.assertEqual([], issues)

    def test_xml_007_fail(self):
        code_sample = (
            "<hdlworker language=\"vhdl\" spec=\"component-spec\">\n" +
            "  <streaminterface name=\"input\" datawidth=\"8\"/>\n" +
            "  <streaminterface name=\"output\" datawidth=\"8\"/>\n" +
            "  <property name=\"a_worker_property\" parameter=\"true\" " +
            "initial=\"true\"/>\n" +
            "</hdlworker>\n")
        with open(self.test_file_path, "w+") as source_code_file:
            source_code_file.write(code_sample)

        code_checker = XmlCodeChecker(self.test_file_path)
        code_checker._read_in_code()

        with unittest.mock.patch(worker_description_patch_string,
                                 return_value=True):
            _, issues = code_checker.test_xml_007()

        self.assertEqual(1, len(issues))
        self.assertEqual(4, issues[0]["line"])

    def test_xml_008_pass(self):
        code_sample = ""
        with open(self.test_file_path, "w+") as source_code_file:
            source_code_file.write(code_sample)

        code_checker = XmlCodeChecker(self.test_file_path)
        code_checker._read_in_code()

        with unittest.mock.patch(worker_description_patch_string,
                                 return_value=True):
            _, issues = code_checker.test_xml_008()

        self.assertEqual(0, len(issues))

    def test_xml_009_pass(self):
        code_sample = (
            "<build>\n" +
            "  <parameter name=\"a_parameter\" value=\"5\"/>\n" +
            "</build>\n")
        with open(self.test_file_path, "w+") as source_code_file:
            source_code_file.write(code_sample)

        code_checker = XmlCodeChecker(self.test_file_path)
        code_checker._read_in_code()

        with unittest.mock.patch(build_file_patch_string,
                                 return_value=True):
            _, issues = code_checker.test_xml_009()

        self.assertEqual([], issues)

    def test_xml_009_fail(self):
        code_checker = XmlCodeChecker(self.test_file_path)
        code_checker._read_in_code()

        with unittest.mock.patch(build_file_patch_string,
                                 return_value=True):
            _, issues = code_checker.test_xml_009()

        self.assertEqual(1, len(issues))

    def test_xml_010_pass(self):
        code_sample = (
            "<componentspec>\n" +
            "  <port name=\"input\" producer=\"false\" " +
            "protocol=\"short_v1_protocol\"/>\n" +
            " <port name=\"output\" producer=\"true\" " +
            "protocol=\"short_v1_protocol\"/>\n" +
            " <property name=\"a_property\" type=\"uchar\" " +
            "writable=\"true\" default=\"0\" " +
            "description=\"Describes the property.\"/>\n" +
            "</componentspec>\n")
        with open(self.test_file_path, "w+") as source_code_file:
            source_code_file.write(code_sample)

        code_checker = XmlCodeChecker(self.test_file_path)
        code_checker._read_in_code()

        with unittest.mock.patch(component_spec_patch_string,
                                 return_value=True):
            _, issues = code_checker.test_xml_010()

        self.assertEqual([], issues)

    def test_xml_010_fail(self):
        code_sample = (
            "<componentspec name=\"some_different_name\">\n" +
            "  <port name=\"input\" producer=\"false\" " +
            "protocol=\"short_v1_protocol\"/>\n" +
            " <port name=\"output\" producer=\"true\" " +
            "protocol=\"short_v1_protocol\"/>\n" +
            " <property name=\"a_property\" type=\"uchar\" " +
            "writable=\"true\" default=\"0\" " +
            "description=\"Describes the property.\"/>\n" +
            "</componentspec>\n")
        with open(self.test_file_path, "w+") as source_code_file:
            source_code_file.write(code_sample)

        code_checker = XmlCodeChecker(self.test_file_path)
        code_checker._read_in_code()

        with unittest.mock.patch(component_spec_patch_string,
                                 return_value=True):
            _, issues = code_checker.test_xml_010()

        self.assertEqual(1, len(issues))
        self.assertEqual(1, issues[0]["line"])

    def test_xml_011_pass(self):
        code_sample = (
            "<componentspec>\n" +
            " <property name=\"a_property\" type=\"uchar\" " +
            "writable=\"true\" default=\"0\" " +
            "description=\"Describes the property.\"/>\n" +
            "  <port name=\"input\" producer=\"false\" " +
            "protocol=\"short_v1_protocol\"/>\n" +
            " <port name=\"output\" producer=\"true\" " +
            "protocol=\"short_v1_protocol\"/>\n" +
            "</componentspec>\n")
        with open(self.test_file_path, "w+") as source_code_file:
            source_code_file.write(code_sample)

        code_checker = XmlCodeChecker(self.test_file_path)
        code_checker._read_in_code()

        with unittest.mock.patch(component_spec_patch_string,
                                 return_value=True):
            _, issues = code_checker.test_xml_011()

        self.assertEqual([], issues)

    def test_xml_011_fail_outputs_first(self):
        code_sample = (
            "<componentspec>\n" +
            " <port name=\"output\" producer=\"true\" " +
            "protocol=\"short_v1_protocol\"/>\n" +
            " <property name=\"a_property\" type=\"uchar\" " +
            "writable=\"true\" default=\"0\" " +
            "description=\"Describes the property.\"/>\n" +
            "  <port name=\"input\" producer=\"false\" " +
            "protocol=\"short_v1_protocol\"/>\n" +
            "</componentspec>\n")
        with open(self.test_file_path, "w+") as source_code_file:
            source_code_file.write(code_sample)

        code_checker = XmlCodeChecker(self.test_file_path)
        code_checker._read_in_code()

        with unittest.mock.patch(component_spec_patch_string,
                                 return_value=True):
            _, issues = code_checker.test_xml_011()

        # Two issues are expected here, as output first places it both
        # before the input port and before the property tag
        self.assertEqual(2, len(issues))
        self.assertEqual(3, issues[0]["line"])
        self.assertEqual(4, issues[1]["line"])

    def test_xml_011_fail_inputs_first(self):
        code_sample = (
            "<componentspec>\n" +
            "  <port name=\"input\" producer=\"false\" " +
            "protocol=\"short_v1_protocol\"/>\n" +
            " <property name=\"a_property\" type=\"uchar\" " +
            "writable=\"true\" default=\"0\" " +
            "description=\"Describes the property.\"/>\n" +
            " <port name=\"output\" producer=\"true\" " +
            "protocol=\"short_v1_protocol\"/>\n" +
            "</componentspec>\n")
        with open(self.test_file_path, "w+") as source_code_file:
            source_code_file.write(code_sample)

        code_checker = XmlCodeChecker(self.test_file_path)
        code_checker._read_in_code()

        with unittest.mock.patch(component_spec_patch_string,
                                 return_value=True):
            _, issues = code_checker.test_xml_011()

        self.assertEqual(1, len(issues))
        self.assertEqual(3, issues[0]["line"])

    def test_xml_011_fail_input_last(self):
        code_sample = (
            "<componentspec>\n" +
            " <property name=\"a_property\" type=\"uchar\" " +
            "writable=\"true\" default=\"0\" " +
            " <port name=\"output\" producer=\"true\" " +
            "protocol=\"short_v1_protocol\"/>\n" +
            "description=\"Describes the property.\"/>\n" +
            "  <port name=\"input\" producer=\"false\" " +
            "protocol=\"short_v1_protocol\"/>\n" +
            "</componentspec>\n")
        with open(self.test_file_path, "w+") as source_code_file:
            source_code_file.write(code_sample)

        code_checker = XmlCodeChecker(self.test_file_path)
        code_checker._read_in_code()

        with unittest.mock.patch(component_spec_patch_string,
                                 return_value=True):
            _, issues = code_checker.test_xml_011()

        self.assertEqual(1, len(issues))
        self.assertEqual(4, issues[0]["line"])

    def test_xml_012_pass(self):
        code_sample = (
            "<componentspec>\n" +
            "  <port name=\"input\" producer=\"false\" " +
            "protocol=\"short_v1_protocol\"/>\n" +
            " <port name=\"output\" producer=\"true\" " +
            "protocol=\"short_v1_protocol\"/>\n" +
            " <property name=\"a_property\" type=\"uchar\" " +
            "writable=\"true\" default=\"0\" " +
            "description=\"Describes the property.\"/>\n" +
            "</componentspec>\n")
        with open(self.test_file_path, "w+") as source_code_file:
            source_code_file.write(code_sample)

        code_checker = XmlCodeChecker(self.test_file_path)
        code_checker._read_in_code()

        with unittest.mock.patch(component_spec_patch_string,
                                 return_value=True):
            _, issues = code_checker.test_xml_012()

        self.assertEqual([], issues)

    def test_xml_012_fail(self):
        code_sample = (
            "<componentspec>\n" +
            "  <port name=\"input\" producer=\"false\" " +
            "protocol=\"short_v1_protocol\"/>\n" +
            " <port name=\"output\" producer=\"true\" " +
            "protocol=\"short_v1_protocol\"/>\n" +
            " <property name=\"a_property\" type=\"uchar\" " +
            "writable=\"true\" " +
            "description=\"Describes the property.\"/>\n" +
            "</componentspec>\n")
        with open(self.test_file_path, "w+") as source_code_file:
            source_code_file.write(code_sample)

        code_checker = XmlCodeChecker(self.test_file_path)
        code_checker._read_in_code()

        with unittest.mock.patch(component_spec_patch_string,
                                 return_value=True):
            _, issues = code_checker.test_xml_012()

        self.assertEqual(1, len(issues))
        self.assertEqual(4, issues[0]["line"])

    def test_xml_013_pass(self):
        code_sample = (
            "<componentspec>\n" +
            "  <port name=\"input\" producer=\"false\" " +
            "protocol=\"short_v1_protocol\"/>\n" +
            " <port name=\"output\" producer=\"true\" " +
            "protocol=\"short_v1_protocol\"/>\n" +
            " <property name=\"a_property\" type=\"uchar\" " +
            "writable=\"true\" default=\"0\" " +
            "description=\"Describes the property.\"/>\n" +
            "</componentspec>\n")
        with open(self.test_file_path, "w+") as source_code_file:
            source_code_file.write(code_sample)

        code_checker = XmlCodeChecker(self.test_file_path)
        code_checker._read_in_code()

        with unittest.mock.patch(component_spec_patch_string,
                                 return_value=True):
            _, issues = code_checker.test_xml_013()

        self.assertEqual([], issues)

    def test_xml_013_fail(self):
        code_sample = (
            "<componentspec>\n" +
            "  <port name=\"input\" producer=\"false\" " +
            "protocol=\"short_v1_protocol\"/>\n" +
            " <port name=\"output\" producer=\"true\" " +
            "protocol=\"short_v1_protocol\"/>\n" +
            " <property name=\"a_property\" type=\"uchar\" " +
            "writable=\"true\" default=\"0\" " +
            "</componentspec>\n")
        with open(self.test_file_path, "w+") as source_code_file:
            source_code_file.write(code_sample)

        code_checker = XmlCodeChecker(self.test_file_path)
        code_checker._read_in_code()

        with unittest.mock.patch(component_spec_patch_string,
                                 return_value=True):
            _, issues = code_checker.test_xml_013()

        self.assertEqual(1, len(issues))
        self.assertEqual(4, issues[0]["line"])
