#!/usr/bin/env python3

# Test code in base_code_checker.py
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.


import os
import pathlib
import unittest

from ocpi_linter.base_code_checker import BaseCodeChecker


# Set to True and during testing the user must interact with the terminal.
# Cannot be set to True when part of an automated test suite.
USER_INTERACTION = False


class ACodeChecker(BaseCodeChecker):
    def test_000(self):
        return "Test name", ["An issue", "Another issue"]


class TestBaseCodeChecker(unittest.TestCase):
    def setUp(self):
        self.test_file_path = pathlib.Path("test_file.cc")
        self.test_file_path.touch()

        self.test_checker = ACodeChecker(self.test_file_path)

    def tearDown(self):
        os.remove(self.test_file_path)

    @unittest.skipIf(USER_INTERACTION is False,
                     "Skipping user interaction tests")
    def test_lint(self):
        lint_result = self.test_checker.lint()

        self.assertEqual({0: {"name": "Test name", "issue_count": 2}},
                         lint_result)

    def test_check_installed_true(self):
        self.assertTrue(self.test_checker._check_installed("python3"))

    def test_check_installed_false(self):
        print("\nTesting will print an error to the terminal. Safe to ignore.")
        self.assertFalse(self.test_checker._check_installed("some_program"))
