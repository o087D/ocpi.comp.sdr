#!/usr/bin/env python3

# Test code in ask_user.py
#
# This file is protected by Copyright. Please refer to the COPYRIGHT file
# distributed with this source distribution.
#
# This file is part of OpenCPI <http://www.opencpi.org>
#
# OpenCPI is free software: you can redistribute it and/or modify it under the
# terms of the GNU Lesser General Public License as published by the Free
# Software Foundation, either version 3 of the License, or (at your option) any
# later version.
#
# OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
# WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
# A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
# more details.
#
# You should have received a copy of the GNU Lesser General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.


import unittest
import unittest.mock

from ocpi_linter.utilities import ask_user


class TestAskUser(unittest.TestCase):
    def test_bool_y(self):
        with unittest.mock.patch("builtins.input", return_value="y"):
            response = ask_user("Test: Respond with y?", bool)
            self.assertTrue(response)

    def test_bool_yes(self):
        with unittest.mock.patch("builtins.input", return_value="yes"):
            response = ask_user("Test: Respond with yes?", bool)
            self.assertTrue(response)

    def test_bool_y_uppercase(self):
        with unittest.mock.patch("builtins.input", return_value="Y"):
            response = ask_user("Test: Respond with Y?", bool)
            self.assertTrue(response)

    def test_bool_yes_uppercase(self):
        with unittest.mock.patch("builtins.input", return_value="YES"):
            response = ask_user("Test: Respond with YES?", bool)
            self.assertTrue(response)

    def test_bool_n(self):
        with unittest.mock.patch("builtins.input", return_value="n"):
            response = ask_user("Test: Respond with n?", bool)
            self.assertFalse(response)

    def test_bool_no(self):
        with unittest.mock.patch("builtins.input", return_value="no"):
            response = ask_user("Test: Respond with no?", bool)
            self.assertFalse(response)

    def test_bool_n_uppercase(self):
        with unittest.mock.patch("builtins.input", return_value="N"):
            response = ask_user("Test: Respond with N?", bool)
            self.assertFalse(response)

    def test_bool_no_uppercase(self):
        with unittest.mock.patch("builtins.input", return_value="NO"):
            response = ask_user("Test: Respond with No?", bool)
            self.assertFalse(response)

    def test_int(self):
        with unittest.mock.patch("builtins.input", return_value=123):
            response = ask_user("Test: Respond with 123?", int)
            self.assertEqual(123, response)

    def test_float(self):
        with unittest.mock.patch("builtins.input", return_value=1.23):
            response = ask_user("Test: Respond with 1.23?", float)
            self.assertEqual(1.23, response)

    def test_string(self):
        with unittest.mock.patch("builtins.input", return_value="hello"):
            response = ask_user("Test: Respond with 1.23?", str)
            self.assertEqual("hello", response)
