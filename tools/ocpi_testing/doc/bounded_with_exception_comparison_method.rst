.. Description of bounded with exception comparison method

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


.. _bounded_with_exception_comparison:


Bounded with exception comparison method
========================================
Bounded with exception comparison method checks all data values in all messages are within some set standard error bound of the respective reference sample, with up to a set number of samples allowed to have a error difference greater than the standard bound but with an exception bound.

Bounded with exception comparison method is intended for testing with integer output data, as there is a fixed resolution between adjacent values in the integer number system. Bounded with exception comparison is not intended for testing with floating point output data, as the difference between adjacent values changes if the numbers have a large or small magnitude, , in such cases `statistical comparison <statistical_comparison>`_ should be used; however it is noted some floating point output components may still use bounded comparison.

.. _bounded_with_exception_comparison-diagram:

.. figure:: bounded_comparison_with_exception.svg
   :alt: Outline of the range of allowed values for bounded with exception comparison.
   :align: center

   Range of values allowed in relation to the reference for test to pass.

In :numref:`bounded_with_exception_comparison-diagram` :math:`\delta_s` is the set standard bound value; :math:`\delta_e` is the exception bound value; the red crosses are the reference values. Up to a set number of values may be outside :math:`\delta_s` but within :math:`\delta_e` and all other values must be within :math:`\delta_s` for test to pass.

If testing floating point numbers and a positive infinity, negative infinity or not-a-number sample is encountered, this will pass if both the reference and implementation-under-test value are the same.


Parameters
----------

Bound
~~~~~
Used to control the width of the bound for comparison testing.

.. autoattribute:: ocpi_testing._comparison_methods.bounded.BoundedDefaults.BOUND
   :noindex:

Once initialised as a comparison method, as part of a verifier, this is accessed using:

.. code-block:: python

   verifier.comparison[0].BOUND = 4

The above example is for the first output port, index ``0``.

Exception bound
~~~~~~~~~~~~~~~
Used to control the width of the exception bound for comparison testing.

.. autoattribute:: ocpi_testing._comparison_methods.bounded_with_exception.BoundedWithExceptionDefaults.EXCEPTION_BOUND
   :noindex:

Once initialised as a comparison method, as part of a verifier, this is accessed using:

.. code-block:: python

   verifier.comparison[0].EXCEPTION_BOUND = 12

The above example is for the first output port, index ``0``.

Allowed exceptions
~~~~~~~~~~~~~~~~~~

.. autoattribute:: ocpi_testing._comparison_methods.bounded_with_exception.BoundedWithExceptionDefaults.ALLOWED_EXCEPTION_RATE
   :noindex:

Once initialised as a comparison method, as part of a verifier, this is accessed using:

.. code-block:: python

   verifier.comparison[0].ALLOWED_EXCEPTION_RATE = 0.1

The above example is for the first output port, index ``0``.

Wrap round
~~~~~~~~~~

.. autoattribute:: ocpi_testing._comparison_methods.bounded.BoundedDefaults.WRAP_ROUND_VALUES
   :noindex:

.. _bounded_comparison_with_exception_with_wrap-diagram:

.. figure:: bounded_with_exception_comparison_with_wrap.svg
   :alt: Outline of the range of allowed values for bounded comparison with wrap.
   :align: center

   Range of values allowed in relation to the reference for test to pass.

When ``WRAP_ROUND_VALUES`` are set :numref:`bounded_comparison_with_exception_with_wrap-diagram` shows how the bounds allow values near the limit of the ``WRAP_ROUND_VALUES``.

Once initialised as a comparison method, as part of a verifier, this is accessed using:

.. code-block:: python

   verifier.comparison[0].WRAP_ROUND_VALUES = [-128, 127]

The above example is for the first output port, index ``0``.
