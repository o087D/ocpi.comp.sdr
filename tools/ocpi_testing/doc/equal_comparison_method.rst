.. Description of equal comparison method

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


Equal comparison method
=======================
The equal comparison method checks all data values in all messages are equal. Equal in this case is when the ``==`` operator returns ``True`` in Python 3.6.

Wherever possible the equal comparison method should be used. However there are two cases where the equal comparison method is not suitable:

 #. When the output data type is floating point values. Due to the rounding error in handling floating point numbers, exactly equal outputs from both the reference and implementation-under-test is very unlikely.

 #. When comparing data from an implementation-under-test that uses an approximation (e.g. in trigonometric functions). While most approximations are very good, there may be a small difference between the implementation-under-test and reference outputs.
