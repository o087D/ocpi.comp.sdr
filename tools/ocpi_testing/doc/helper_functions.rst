.. Outlines testing helper functions

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


Helper functions
================
``ocpi_testing`` has a number of exposed helper functions for use in generate and verify scripts.

Get generate script arguments (``ocpi_testing.get_generate_arguments``)
-----------------------------------------------------------------------
.. autofunction:: ocpi_testing.get_generate_arguments

Get test seed (``ocpi_testing.get_test_seed``)
----------------------------------------------
.. autofunction:: ocpi_testing.get_test_seed

Case name getter (``ocpi_testing.get_test_case``)
-------------------------------------------------
.. autofunction:: ocpi_testing.get_test_case

Test name / ID to case and subcase (``ocpi_testing.id_to_case``)
----------------------------------------------------------------
.. autofunction:: ocpi_testing.id_to_case
