.. Outlines testing generators

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


Generator classes (``ocpi_testing.generator``)
==============================================
A generator class for each "timed sample" protocol type is available in ``ocpi_testing.generator``. The available generator classes are:

.. toctree::
   :maxdepth: 1

   boolean_generator
   character_generator
   short_generator
   long_generator
   unsigned_character_generator
   unsigned_short_generator
   unsigned_long_generator
   float_generator
   double_generator
   complex_short_generator
   complex_long_generator
   complex_float_generator
   complex_double_generator

Each generator has a method for each expected test case (e.g. typical, property). This test case should be passed as an argument to the generate script by being defined in the OpenCPI test XML file as an argument for the generate script.

An example of using a generator is :doc:`here <example_generate>`.

Test cases
----------
All of the generators support the following test cases and subcases:

 * ``typical``: Generates a sample message with typical data input - as a signal processing library the default is superposition of sinusoidal waves. Has no subcases.

 * ``property``: Generates a sample message with random data, to allow for testing of a property with different values. The subcase should be set to the property name being tested.

 * ``sample``: Generates sample messages with different data types depending on the subcase. The allowed subcases depends on the protocol being tested:

    * For the **boolean** protocol the allowed subcases are ``all_zero`` and ``all_maximum``.

    * For **character**, **short** and **long** protocols the allowed subcases are ``all_zero``, ``all_maximum``, ``all_minimum``, ``large_positive``, ``large_negative`` and ``near_zero``.

    * For **unsigned character**, **unsigned short** and **unsigned long** protocols the allowed subcases are ``all_zero``, ``all_maximum``, ``large_positive``, and ``near_zero``.

    * For **complex short** and **complex long** protocols the allowed subcases are ``all_zero``, ``all_maximum``, ``all_minimum``, ``real_zero``, ``imaginary_zero``, ``large_positive``, ``large_negative`` and ``near_zero``.

    * For **float** and **double** protocols the allowed subcases are ``all_zero``, ``all_maximum``, ``all_minimum``, ``large_positive``, ``large_negative``, ``near_zero``, ``positive_infinity``, ``negative_infinity`` and ``not_a_number``.

    * For **complex float** and **complex double** protocols the allowed subcases are ``all_zero``, ``all_maximum``, ``all_minimum``, ``real_zero``, ``imaginary_zero``, ``large_positive``, ``large_negative``, ``near_zero``, ``positive_infinity``, ``negative_infinity`` and ``not_a_number``.

 * ``sample_other_port``: Generates sample message(s) with random data, for use with multiple input port components when one of the input ports is undergoing ``sample`` testing. Supports same subcases as allowed for ``sample`` tests.

 * ``input_stressing``: Generates random data for use when ``stressormode="full`` is set for an input port.

 * ```input_stressing_other_port``: Generates sample message(s) with random data, for use with multiple input port components when one of the input ports is undergoing ``input_stressing_other_port`` testing. Has no subcases.

 * ``message_size``: Generates sample messages of different size with random data (over a limit of the whole range of supported values). Allowed subcases are ``shortest``, ``longest`` and ``different_sizes``.

 * ``time``: Generates a combination of messages with a sample or time opcode to test handling of time opcode messages. Allowed subcases are: ``zero``, ``positive``, ``maximum`` and ``consecutive``.

 * ``time_other_port``: Generates sample message(s) with random data, for use with multiple input port components when one of the input ports is undergoing ``time`` testing. Supports same subcases as allowed for ``time`` tests.

 * ``sample_interval``: Generates a combination of messages with a sample or sample interval opcode to test handling of sample interval opcode messages. Allowed subcases are: ``zero``, ``positive``, ``maximum`` and ``consecutive``.

 * ``sample_interval_other_port``: Generates sample message(s) with random data, for use with multiple input port components when one of the input ports is undergoing ``sample_interval`` testing. Supports same subcases as allowed for ``sample_interval`` tests.

 * ``flush``: Generates a combination of messages with a sample or flush opcode to test handling of flush opcode messages. Allowed subcases are: ``single`` and ``consecutive``.

 * ``flush_other_port``: Generates sample message(s) with random data, for use with multiple input port components when one of the input ports is undergoing ``flush`` testing. Supports same subcases as allowed for ``flush`` tests.

 * ``discontinuity``: Generates a combination of messages with a sample or discontinuity opcode to test handling of discontinuity opcode messages. Allowed subcases are: ``single`` and ``consecutive``.

 * ``discontinuity_other_port``: Generates sample message(s) with random data, for use with multiple input port components when one of the input ports is undergoing ``discontinuity`` testing. Supports same subcases as allowed for ``discontinuity`` tests.

 * ``metadata``: Generates a combination of messages with a sample or sample metadata opcode to test handling of sample metadata opcode messages. Allowed subcases are: ``zero``, ``positive``, ``maximum`` and ``consecutive``.

 * ``metadata_other_port``: Generates sample message(s) with random data, for use with multiple input port components when one of the input ports is undergoing ``sample_metadata`` testing. Supports same subcases as allowed for ``sample_metadata`` tests.

 * ``soak``: Generates a random number of messages with random opcodes and random data content, to test combinations of messages. Allowed subcases are ``sample_only`` and ``all_opcodes``.

 * ``custom``: A test cases provided to be overloaded by a component specific generator, where needed. The allowed subcases will be those defined in the overloaded method.

Custom generators
-----------------
If a particular component requires a different type of data to be generated for a test case then a custom generator should be defined, which inherits from the protocol type generate. With the test case that needs a custom behaviour overloaded.

Each test case method takes a seed and a subcase; the seed is an integer used to seed random number generation so repeated runs of the same test use the same value which ensure runs which find bug find these on each test run, and a subcase which should be set as a test property in the OpenCPI test XML file.

If additional test cases are needed for a custom generator then these need to be added to the ``self.CASES`` dictionary in the ``self.__init__()`` method, after ``super().__init__`` has been called.
