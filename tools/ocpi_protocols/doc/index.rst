.. User guide for ocpi_protocols library

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


Timed sample protocols module (``ocpi_protocols``)
==================================================
``ocpi_protocols`` is a Python module for reading and writing files generated and consumed by OpenCPI's ``file_read`` and ``file_write`` components in the protocol streams defined in ``ocpi.core``.

``ocpi_protocols`` includes the command line tool ``ocpi_protocol_view`` for inspecting messages-in-file files.

This module contains four classes for file manipulation:

.. toctree::
   :maxdepth: 1

   write_stream_file
   write_messages_file
   parse_stream_file
   parse_messages_file
   ocpi_protocol_view


Installing
----------
``ocpi_protocols`` is written for and tested with Python 3.6 - newer versions may work, older versions are not expected to. Python must be installed before attempting to install ``ocpi_protocols``.

To install ``ocpi_protocols`` use the command:

.. code-block:: bash

   pip3 install --user ./

Alternatively, when ``pip3`` is not installed:

.. code-block:: bash

   python3 setup.py install

Depending on your set-up you may need to run the above command as ``sudo``.

Use
---

In scripts
~~~~~~~~~~
To use ``ocpi_protocols`` within a Python script see the pages for the different parsers and readers linked above.

At the command line
~~~~~~~~~~~~~~~~~~~
To use ``ocpi_protocol_view`` on the command line, commands have the general form:

.. code-block:: bash

   ocpi_protocol_view [OPTIONS] PROTOCOL FILE [FILES]

Options and modes are are outlined on the :ref:`Timed samples protocol viewer pages <ocpi_protocol_view>` or with the ``-h`` / ``--help`` option.

Requirements
------------
``ocpi_protocols`` requires:

 * Python 3.6.


Library testing
---------------
``ocpi_protocols`` has been tested using the Python's ``unittest`` module. Tests are most easily run by using the following command in the top most ``ocpi_protocols`` directory:

.. code-block:: python

   python3 -m unittest
