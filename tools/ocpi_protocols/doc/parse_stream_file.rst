.. User guide for ocpi_protocols.ParseStreamFile

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


Parse stream file (``ocpi_protocols.ParseStreamFile``)
======================================================
``ParseStreamFile`` is for reading files that have been written using OpenCPI's file write component with ``messagesinfile`` set to false (or ``ocpi_protocols.WriteStreamFile``). If the file to be read contains messages, and not streamed data, then the :doc:`messages parser <parse_messages_file>` should be used instead.

``ParseStreamFile`` is expected to be used as a context manager using the ``with`` keyword in a similar way to the ``open()`` function. The attribute ``headers`` and ``messages_data`` can be iterated through and indexed to read the messages file. For example:

.. code-block:: python

   import ocpi_protocols

   with ocpi_protocols.ParseStreamFile("~/protocol.file", "short") as protocol_file:
       for sample in protocol_file.data:
           print("Sample:", sample)


Structure
---------
.. autoclass:: ocpi_protocols.ParseStreamFile
   :members:
