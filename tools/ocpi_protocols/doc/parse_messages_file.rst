.. User guide for ocpi_protocols.ParseMessagesFile

.. This file is protected by Copyright. Please refer to the COPYRIGHT file
   distributed with this source distribution.

   This file is part of OpenCPI <http://www.opencpi.org>

   OpenCPI is free software: you can redistribute it and/or modify it under the
   terms of the GNU Lesser General Public License as published by the Free
   Software Foundation, either version 3 of the License, or (at your option) any
   later version.

   OpenCPI is distributed in the hope that it will be useful, but WITHOUT ANY
   WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR
   A PARTICULAR PURPOSE. See the GNU Lesser General Public License for
   more details.

   You should have received a copy of the GNU Lesser General Public License
   along with this program. If not, see <http://www.gnu.org/licenses/>.


Parse message file (``ocpi_protocols.ParseMessagesFile``)
=========================================================
``ParseMessageFile`` is for reading files that have been written using OpenCPI's file write component with ``messagesinfile`` is set to true (or ``ocpi_protocols.WriteMessagesFile``). If the file to be read contains only stream data, and not messages, then the :doc:`stream parser <parse_stream_file>` should be used instead.

``ParseMessagesFile`` is expected to be used as a context manager using the ``with`` keyword in a similar way to the ``open()`` function. The attribute ``headers`` and ``messages_data`` can be iterated through and indexed to read the messages file. For example:

.. code-block:: python

   import ocpi_protocols

   with ocpi_protocols.ParseMessagesFile("~/protocol.file", "short") as protocol_file:
       print("Messages in file:", protocol_file.headers)
       print("First message's data:", protocol_file.messages_data[0])

       stream_data = protocol_file.get_stream_data()


Structure
---------
.. autoclass:: ocpi_protocols.ParseMessagesFile
   :members:
